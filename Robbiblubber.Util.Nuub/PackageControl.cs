﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements the solution control.</summary>
    public sealed partial class PackageControl: UserControl, IControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tree node.</summary>
        private TreeNode _Node;

        /// <summary>Package.</summary>
        private Package _Package;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public PackageControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates with default values.</summary>
        private void _UpdateDefaults()
        {
            _UpdateDefaults(_TextDescription, _Package.Solution.Description);

            _UpdateDefaults(_TextVersion, _Package.Solution.Version.ToVersionString());

            _UpdateDefaults(_TextAuthors, _Package.Solution.Authors);
            _UpdateDefaults(_TextOwners, _Package.Solution.Owners );
            _UpdateDefaults(_TextCopyright, _Package.Solution.Copyright);
            _UpdateDefaults(_TextLicense, _Package.Solution.LicenseURL);

            _UpdateDefaults(_TextProjectURL, _Package.Solution.ProjectURL);
            _UpdateDefaults(_TextRepositoryURL, _Package.Solution.RepositoryURL);
            _UpdateDefaults(_TextRepositoryType, _Package.Solution.RepositoryType);

            _UpdateDefaults(_TextTags, _Package.Solution.Tags);
            _UpdateDefaults(_TextIcon, _Package.Solution.IconURL);
            _UpdateDefaults(_TextRelNotes, _Package.Solution.ReleaseNotes);
        }


        /// <summary>Updates a text box with default values.</summary>
        /// <param name="textBox">Text box.</param>
        /// <param name="defaultValue">Default value.</param>
        private void _UpdateDefaults(TextBox textBox, string defaultValue)
        {
            if(string.IsNullOrWhiteSpace(textBox.Text)) { textBox.Text = defaultValue; }

            if(textBox.Text.Trim() == defaultValue.Trim())
            {
                textBox.ForeColor = SystemColors.ControlDark;
            }
            else
            {
                textBox.ForeColor = SystemColors.ControlText;
            }
        }


        /// <summary>Determines if a value has been modified for a text box.</summary>
        /// <param name="textBox">Text box.</param>
        /// <param name="current">Current value.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Returns TRUE if the value is modified, otherwise returns FALSE.</returns>
        private bool _Modified(TextBox textBox, string current, string defaultValue)
        {
            if(string.IsNullOrWhiteSpace(textBox.Text))
            {
                return !(string.IsNullOrWhiteSpace(current) || (current.Trim() == defaultValue.Trim()));
            }

            if(string.IsNullOrWhiteSpace(current)) { return (textBox.Text.Trim() != defaultValue.Trim()); }

            return (textBox.Text != current);
        }


        /// <summary>Gets a field value regarding defaults.</summary>
        /// <param name="textBox">Text box.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value.</returns>
        private string _ActualValue(TextBox textBox, string defaultValue)
        {
            if(string.IsNullOrWhiteSpace(textBox.Text))    { return ""; }
            if(textBox.Text.Trim() == defaultValue.Trim()) { return ""; }

            return textBox.Text;
        }

        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IControl                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set
            {
                _Node = value;
                if(value == null) return;

                _Package = ((Package) Node.Tag);
                Reload();
            }
        }


        /// <summary>Gets if the contents have been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_TextName.Text != _Package.Name) return true;

                if(_Modified(_TextVersion, (_Package.Version == null) ? "" : _Package.Version.ToVersionString(), _Package.Solution.Version.ToVersionString())) return true;

                if(_Modified(_TextAuthors, _Package.Authors, _Package.Solution.Authors)) return true;
                if(_Modified(_TextOwners, _Package.Owners, _Package.Solution.Owners)) return true;
                if(_Modified(_TextCopyright, _Package.Copyright, _Package.Solution.Copyright)) return true;
                if(_Modified(_TextLicense, _Package.LicenseURL, _Package.Solution.LicenseURL)) return true;

                if(_Modified(_TextProjectURL, _Package.ProjectURL, _Package.Solution.ProjectURL)) return true;
                if(_Modified(_TextRepositoryURL, _Package.RepositoryURL, _Package.Solution.RepositoryURL)) return true;
                if(_Modified(_TextRepositoryType, _Package.RepositoryType, _Package.Solution.RepositoryType)) return true;

                if(_Modified(_TextTags, _Package.Tags, _Package.Solution.Tags)) return true;
                if(_Modified(_TextIcon, _Package.IconURL, _Package.Solution.IconURL)) return true;
                if(_Modified(_TextRelNotes, _Package.ReleaseNotes, _Package.Solution.ReleaseNotes)) return true;

                if(_Modified(_TextDescription, _Package.Description, _Package.Solution.Description)) return true;

                return false;
            }
        }


        /// <summary>Saves the contents.</summary>
        /// <param name="name">New name.</param>
        /// <returns>Returns TRUE if the object has been saved, FALSE if an error has occured.</returns>
        public bool Save(string name = null)
        {
            if(name != null) { _TextName.Text = name; }

            _Package.Name = _TextName.Text;
            _Package.Description = _ActualValue(_TextDescription, _Package.Solution.Description);

            try
            {
                _Package.Version = new Version(_TextVersion.Text);
            }
            catch(Exception)
            {
                MessageBox.Show("nuub::udiag.save.vparseerr".Localize("The object could not be saved. The version in invalid."), "nuub::udiag.save.caption".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            _Package.Authors = _ActualValue(_TextAuthors, _Package.Solution.Authors);
            _Package.Owners = _ActualValue(_TextOwners, _Package.Solution.Owners);
            _Package.Copyright = _ActualValue(_TextCopyright, _Package.Solution.Copyright);
            _Package.LicenseURL = _ActualValue(_TextLicense, _Package.Solution.LicenseURL);

            _Package.ProjectURL = _ActualValue(_TextProjectURL, _Package.Solution.ProjectURL);
            _Package.RepositoryURL = _ActualValue(_TextRepositoryURL, _Package.Solution.RepositoryURL);
            _Package.RepositoryType = _ActualValue(_TextRepositoryType, _Package.Solution.RepositoryType);

            _Package.Tags = _ActualValue(_TextTags, _Package.Solution.Tags);
            _Package.IconURL = _ActualValue(_TextIcon, _Package.Solution.IconURL);
            _Package.ReleaseNotes = _ActualValue(_TextRelNotes, _Package.Solution.ReleaseNotes);

            try
            {
                _Package.Save();

                if(name == null) { _Node.Text = _Package.Name; }
            }
            catch(Exception) { MessageBox.Show("nuub::udiag.save.uniqerr".Localize("The object could not be saved. The name already exists in this context."), "nuub::udiag.save.caption".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error); }

            _ButtonSave.Enabled = false;

            return true;
        }


        /// <summary>Refreshes the contents.</summary>
        public void Reload()
        {
            _TextName.Text = _Package.Name;
            
            _TextDescription.Text = _Package.Description;
            _TextVersion.Text = ((_Package.Version == null) ? "" : _Package.Version.ToVersionString());

            _TextAuthors.Text = _Package.Authors;
            _TextOwners.Text = _Package.Owners;
            _TextCopyright.Text = _Package.Copyright;
            _TextLicense.Text = _Package.LicenseURL;

            _TextProjectURL.Text = _Package.ProjectURL;
            _TextRepositoryURL.Text = _Package.RepositoryURL;
            _TextRepositoryType.Text = _Package.RepositoryType;

            _TextTags.Text = _Package.Tags;
            _TextIcon.Text = _Package.IconURL;
            _TextRelNotes.Text = _Package.ReleaseNotes;
            
            _Node.Text = _Package.Name;

            _UpdateDefaults();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Resize.</summary>
        private void _Resize(object sender, EventArgs e)
        {
            _TextName.Width = (Width - 77);
            _TabPages.Width = (Width - 85);
            _TabPages.Height = (Height - 217);

            _TextAuthors.Width = _TextOwners.Width = _TextCopyright.Width = _TextLicense.Width =
                                 _TextProjectURL.Width = _TextRepositoryURL.Width = _TextRepositoryType.Width =
                                 _TextTags.Width = _TextIcon.Width = _TextRelNotes.Width =
                                 _TextDescription.Width = (Width - 131);
            _TextRelNotes.Height = (Height - 413);
            _TextDescription.Height = (Height - 311);
        }


        /// <summary>Text changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            _ButtonSave.Enabled = Modified;
        }


        /// <summary>Button "Save" click.</summary>
        private void _ButtonSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>Button "Reload" click.</summary>
        private void _ButtonReload_Click(object sender, EventArgs e)
        {
            Reload();
        }


        /// <summary>Control entered.</summary>
        private void _Entered(object sender, EventArgs e)
        {
            ((TextBox) sender).ForeColor = SystemColors.ControlText;
        }


        /// <summary>Control left.</summary>
        private void _Left(object sender, EventArgs e)
        {
            _UpdateDefaults();
        }
    }
}
