﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>Executable items implement this interface.</summary>
    public interface IExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Builds this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        void Build(ExecutionMetadata meta);


        /// <summary>Deploys this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        void Deploy(ExecutionMetadata meta);
    }
}
