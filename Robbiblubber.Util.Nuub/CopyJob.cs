﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a copy job.</summary>
    public sealed class CopyJob: IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Parent folder.</param>
        /// <param name="re">Reader.</param>
        public CopyJob(Folder folder, IDataReader re)
        {
            Folder = folder;

            ID = re.GetString(0);
            Name = re.GetString(1);
            Description = re.GetString(2);
            Files = re.GetString(3);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Parent folder.</param>
        public CopyJob(Folder folder)
        {
            Folder = folder;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";
            Files = "";

            Save();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="folder">Parent folder.</param>
        /// <param name="template">Template.</param>
        public CopyJob(Folder folder, CopyJob template)
        {
            Folder = folder;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            Files = template.Files;

            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the copy job files.</summary>
        public string Files
        {
            get; set;
        }


        /// <summary>Gets the target files as an array.</summary>
        public string[] TargetFiles
        {
            get
            {
                List<string> rval = new List<string>();
                foreach(string i in Files.Replace("\r\n", "\n").Replace("\r", "\n").Split('\n'))
                {
                    if(!string.IsNullOrWhiteSpace(i)) { rval.Add(i.Trim()); }
                }

                return rval.ToArray();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates the folder in a build process.</summary>
        /// <param name="path">Folder path.</param>
        /// <param name="meta">Metadata.</param>
        public void Build(string path, ExecutionMetadata meta)
        {
            foreach(string i in Files.Replace("\r\n", "\n").Split('\n'))
            {
                if(string.IsNullOrWhiteSpace(i)) continue;

                foreach(string k in Directory.GetFiles(Path.GetDirectoryName(i), Path.GetFileName(i)))
                {
                    File.Copy(k, path + @"\" + Path.GetFileName(k));
                    meta.Target.AppendLog("nuub::build.copiedfile".Localize("Copied file \"$(1)\".").Replace("$(1)", Path.GetFileName(k)));
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the folder ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the package name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the package description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Folder.Repository; }
        }


        /// <summary>Gets the parent folder for this folder.</summary>
        public Folder Folder
        {
            get; private set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            IDbCommand cmd = Repository._Connection.CreateCommand("DELETE FROM COPYJOBS WHERE ID = :id");
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Folder.Jobs._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            if(ID == null)
            {
                ID = StringOp.Unique();

                IDbCommand cmd = Repository._Connection.CreateCommand("INSERT INTO COPYJOBS (ID, KFOLDER, NAME, DESCRIPTION, FILES) VALUES (:id, :folder, :name, :descr, :files)");
                cmd.AddParameter(":id", ID);
                cmd.AddParameter(":folder", Folder.ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":files", Files);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Folder.Jobs._Items.Add(ID, this);
            }
            else
            {
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE COPYJOBS SET NAME = :name, DESCRIPTION = :descr, FILES = :files WHERE ID = :id");
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":files", Files);
                cmd.AddParameter(":id", ID);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);
            }
        }

        
        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Folder)) return null;

            return new CopyJob((Folder) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is Folder)) return null;
            if(((Folder) target).ID == Folder.ID) return null;

            Folder.Jobs._Items.Remove(ID);

            Folder = ((Folder) target);
            IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE COPYJOBS SET KFOLDER = :folder WHERE ID = :id");
            cmd.AddParameter(":folder", Folder.ID);
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Disposal.Recycle(cmd);

            Folder.Jobs._Items.Add(ID, this);

            return this;
        }
    }
}
