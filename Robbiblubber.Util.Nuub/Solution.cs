﻿using Robbiblubber.Util.Localization.Controls;
using System;
using System.Data;

using Robbiblubber.Data;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a Nuub solution.</summary>
    public sealed class Solution: IItem, IExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Version.</summary>
        private Version _Version;


        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="repository">Repository</param>
        public Solution(Repository repository, IDataReader re)
        {
            Repository = repository;

            ID = re.GetString(0);
            Name = re.GetString(1);
            Description = re.GetString(2);

            _Version = new Version(re.GetString(3));

            Authors = re.GetString(4);
            Owners = re.GetString(5);
            Copyright = re.GetString(6);
            LicenseURL = re.GetString(7);

            ProjectURL = re.GetString(8);
            RepositoryURL = re.GetString(9);
            RepositoryType = re.GetString(10);

            Tags = re.GetString(11);
            IconURL = re.GetString(12);
            ReleaseNotes = re.GetString(13);

            Packages = new PackageList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="repository">Repository</param>
        public Solution(Repository repository)
        {
            Repository = repository;

            ID = null;
            Name = Repository._GetUniqueName("nuub::udiag.unnamed".Localize("Unnamed"), "SOLUTIONS");
            Description = "";

            _Version = new Version("1.0.0");

            Authors = Owners = Copyright = LicenseURL = "";

            ProjectURL = RepositoryURL = RepositoryType = "";

            Tags = IconURL = ReleaseNotes = "";

            Save();

            Packages = new PackageList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="template">Template solution.</param>
        /// <param name="repository">Repository</param>
        public Solution(Repository repository, Solution template)
        {
            Repository = repository;

            ID = null;
            Name = Repository._GetUniqueName(template.Name, "SOLUTIONS");
            Description = template.Description;

            Version = new Version(template.Version.ToVersionString());

            Authors = template.Authors;
            Owners = template.Owners;
            Copyright = template.Copyright;
            LicenseURL = template.LicenseURL;

            ProjectURL = template.ProjectURL;
            RepositoryURL = template.RepositoryURL;
            RepositoryType = template.RepositoryType;

            Tags = template.Tags;
            IconURL = template.IconURL;
            ReleaseNotes = template.ReleaseNotes;

            Save();

            Packages = new PackageList(this);

            foreach(Package i in template.Packages) { new Package(this, i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the solution version.</summary>
        public Version Version
        {
            get { return _Version; }
            set
            {
                foreach(Package i in Packages)
                {
                    if(i.Version == _Version)
                    {
                        i.Version = value;
                        i.Save();
                    }
                }

                _Version = value;
            }
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Authors
        {
            get; set;
        }


        /// <summary>Gets or sets the solution owners.</summary>
        public string Owners
        {
            get; set;
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Copyright
        {
            get; set;
        }


        /// <summary>Gets or sets the solution license URL.</summary>
        public string LicenseURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution project URL.</summary>
        public string ProjectURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution icon URL.</summary>
        public string IconURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution tags.</summary>
        public string Tags
        {
            get; set;
        }


        /// <summary>Gets or sets the solution release notes.</summary>
        public string ReleaseNotes
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository URL.</summary>
        public string RepositoryURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository type.</summary>
        public string RepositoryType
        {
            get; set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get; private set;
        }


        /// <summary>Gets the packages for this solution.</summary>
        public PackageList Packages
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a package to the solution.</summary>
        /// <returns>Package.</returns>
        public Package AddPackage()
        {
            return new Package(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the solution ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the solution name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the solution description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            IDbCommand cmd = Repository._Connection.CreateCommand("DELETE FROM SOLUTIONS WHERE ID = :id");
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Repository.Solutions._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            if(ID == null)
            {
                ID = StringOp.Unique();

                IDbCommand cmd = Repository._Connection.CreateCommand("INSERT INTO SOLUTIONS (ID, NAME, DESCRIPTION, VERSION, AUTHORS, OWNERS, COPYRIGHT, LICENSE_URL, PROJECT_URL, REP_URL, REP_TYPE, TAGS, ICON_URL, RELEASE_NOTES) VALUES (:id, :name, :descr, :v, :authors, :owners, :copyright, :licurl, :prjurl, :repurl, :reptype, :tags, :icon, :relnotes)");
                cmd.AddParameter(":id", ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":v", Version.ToVersionString());

                cmd.AddParameter(":authors", Authors);
                cmd.AddParameter(":owners", Owners);
                cmd.AddParameter(":copyright", Copyright);
                cmd.AddParameter(":licurl", LicenseURL);

                cmd.AddParameter(":prjurl", ProjectURL);
                cmd.AddParameter(":repurl", RepositoryURL);
                cmd.AddParameter(":reptype", RepositoryType);

                cmd.AddParameter(":tags", Tags);
                cmd.AddParameter(":icon", IconURL);
                cmd.AddParameter(":relnotes", ReleaseNotes);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Repository.Solutions._Items.Add(ID, this);
            }
            else
            {
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE SOLUTIONS SET NAME = :name, DESCRIPTION = :descr, VERSION = :v, AUTHORS = :authors, OWNERS = :owners, COPYRIGHT = :copyright, LICENSE_URL = :licurl, PROJECT_URL = :prjurl, REP_URL = :repurl, REP_TYPE = :reptype, TAGS = :tags, ICON_URL = :icon, RELEASE_NOTES = :relnotes WHERE ID = :id");
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);

                cmd.AddParameter(":v", Version.ToVersionString());

                cmd.AddParameter(":authors", Authors);
                cmd.AddParameter(":owners", Owners);
                cmd.AddParameter(":copyright", Copyright);
                cmd.AddParameter(":licurl", LicenseURL);

                cmd.AddParameter(":prjurl", ProjectURL);
                cmd.AddParameter(":repurl", RepositoryURL);
                cmd.AddParameter(":reptype", RepositoryType);

                cmd.AddParameter(":tags", Tags);
                cmd.AddParameter(":icon", IconURL);
                cmd.AddParameter(":relnotes", ReleaseNotes);

                cmd.AddParameter(":id", ID);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);
            }
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Repository)) return null;

            return new Solution((Repository) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        IItem IItem.MoveTo(IItem target)
        {
            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExecutable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Builds this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Build(ExecutionMetadata meta)
        {
            foreach(Package i in Packages) { i.Build(meta); }
        }


        /// <summary>Deploys this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Deploy(ExecutionMetadata meta)
        {
            foreach(Package i in Packages) { i.Deploy(meta); }
        }
    }
}
