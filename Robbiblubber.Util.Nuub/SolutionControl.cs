﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements the solution control.</summary>
    public sealed partial class SolutionControl: UserControl, IControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tree node.</summary>
        private TreeNode _Node;

        /// <summary>Solution.</summary>
        private Solution _Solution;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SolutionControl()
        {
            InitializeComponent();
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IControl                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set
            {
                _Node = value;
                if(value == null) return;

                _Solution = ((Solution) Node.Tag);
                Reload();
            }
        }


        /// <summary>Gets if the contents have been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_TextName.Text != _Solution.Name) return true;
                if(_TextVersion.Text != _Solution.Version.ToVersionString()) return true;

                if(_TextAuthors.Text != _Solution.Authors) return true;
                if(_TextOwners.Text != _Solution.Owners) return true;
                if(_TextCopyright.Text != _Solution.Copyright) return true;
                if(_TextLicense.Text != _Solution.LicenseURL) return true;

                if(_TextProjectURL.Text != _Solution.ProjectURL) return true;
                if(_TextRepositoryURL.Text != _Solution.RepositoryURL) return true;
                if(_TextRepositoryType.Text != _Solution.RepositoryType) return true;

                if(_TextTags.Text != _Solution.Tags) return true;
                if(_TextIcon.Text != _Solution.IconURL) return true;
                if(_TextRelNotes.Text != _Solution.ReleaseNotes) return true;

                if(_TextDescription.Text != _Solution.Description) return true;

                return false;
            }
        }


        /// <summary>Saves the contents.</summary>
        /// <param name="name">New name.</param>
        /// <returns>Returns TRUE if the object has been saved, FALSE if an error has occured.</returns>
        public bool Save(string name = null)
        {
            if(name != null) { _TextName.Text = name; }

            _Solution.Name = _TextName.Text;
            _Solution.Description = _TextDescription.Text;

            try
            {
                _Solution.Version = new Version(_TextVersion.Text);
            }
            catch(Exception)
            {
                MessageBox.Show("nuub::udiag.save.vparseerr".Localize("The object could not be saved. The version in invalid."), "nuub::udiag.save.caption".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            _Solution.Authors = _TextAuthors.Text;
            _Solution.Owners = _TextOwners.Text;
            _Solution.Copyright = _TextCopyright.Text;
            _Solution.LicenseURL = _TextLicense.Text;

            _Solution.ProjectURL = _TextProjectURL.Text;
            _Solution.RepositoryURL = _TextRepositoryURL.Text;
            _Solution.RepositoryType = _TextRepositoryType.Text;

            _Solution.Tags = _TextTags.Text;
            _Solution.IconURL = _TextIcon.Text;
            _Solution.ReleaseNotes = _TextRelNotes.Text;

            try
            {
                _Solution.Save();

                if(name == null) { _Node.Text = _Solution.Name; }
            }
            catch(Exception) { MessageBox.Show("nuub::udiag.save.uniqerr".Localize("The object could not be saved. The name already exists in this context."), "nuub::udiag.save.caption".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error); }

            _ButtonSave.Enabled = false;

            return true;
        }


        /// <summary>Refreshes the contents.</summary>
        public void Reload()
        {
            _TextName.Text = _Solution.Name;
            _TextDescription.Text = _Solution.Description;
            _TextVersion.Text = _Solution.Version.ToVersionString();

            _TextAuthors.Text = _Solution.Authors;
            _TextOwners.Text = _Solution.Owners;
            _TextCopyright.Text = _Solution.Copyright;
            _TextLicense.Text = _Solution.LicenseURL;

            _TextProjectURL.Text = _Solution.ProjectURL;
            _TextRepositoryURL.Text = _Solution.RepositoryURL;
            _TextRepositoryType.Text = _Solution.RepositoryType;

            _TextTags.Text = _Solution.Tags;
            _TextIcon.Text = _Solution.IconURL;
            _TextRelNotes.Text = _Solution.ReleaseNotes;
            
            _Node.Text = _Solution.Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Resize.</summary>
        private void _Resize(object sender, EventArgs e)
        {
            _TextName.Width = (Width - 77);
            _TabPages.Width = (Width - 85);
            _TabPages.Height = (Height - 217);

            _TextAuthors.Width = _TextOwners.Width = _TextCopyright.Width = _TextLicense.Width =
                                 _TextProjectURL.Width = _TextRepositoryURL.Width = _TextRepositoryType.Width =
                                 _TextTags.Width = _TextIcon.Width = _TextRelNotes.Width =
                                 _TextDescription.Width = (Width - 131);
            _TextRelNotes.Height = (Height - 413);
            _TextDescription.Height = (Height - 311);
        }


        /// <summary>Text changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            _ButtonSave.Enabled = Modified;
        }


        /// <summary>Button "Save" click.</summary>
        private void _ButtonSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>Button "Reload" click.</summary>
        private void _ButtonReload_Click(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
