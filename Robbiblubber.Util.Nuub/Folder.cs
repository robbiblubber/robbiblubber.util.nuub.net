﻿using System;
using System.Data;
using System.IO;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a package source folder.</summary>
    public sealed class Folder: IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="parent">Parent folder.</param>
        /// <param name="re">Reader.</param>
        public Folder(Package package, Folder parent, IDataReader re)
        {
            Package = package;
            Parent = parent;

            ID = re.GetString(0);
            Name = re.GetString(1);
            Description = re.GetString(2);

            Folders = new FolderList(package, this);
            Jobs = new CopyJobList(this);
        }

        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="parent">Parent folder.</param>
        public Folder(Package package, Folder parent = null)
        {
            Package = package;
            Parent = parent;

            ID = null;
            Name = _GetUniqueName("nuub::udiag.unnamed".Localize("Unnamed"));
            Description = "";

            Save();

            Folders = new FolderList(package, this);
            Jobs = new CopyJobList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="parent">Parent folder.</param>
        /// <param name="template">Template.</param>
        public Folder(Package package, Folder parent, Folder template)
        {
            Package = package;
            Parent = parent;

            ID = null;
            Name = _GetUniqueName(template.Name);
            Description = template.Description;

            Save();

            Folders = new FolderList(package, this);
            Jobs = new CopyJobList(this);

            foreach(Folder i in template.Folders) { new Folder(Package, this, i); }
            foreach(CopyJob i in template.Jobs) { new CopyJob(this, i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder for this folder.</summary>
        public Folder Parent
        {
            get; private set;
        }


        /// <summary>Gets or sets the package name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the package description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the child folders for this folder.</summary>
        public FolderList Folders
        {
            get; private set;
        }


        /// <summary>Gets the copy jobs for this folder.</summary>
        public CopyJobList Jobs
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a new child folder to this folder.</summary>
        /// <returns>Folder.</returns>
        public Folder AddFolder()
        {
            return new Folder(Package, this);
        }


        /// <summary>Adds a new copy job to this folder.</summary>
        /// <returns>Folder.</returns>
        public CopyJob AddCopyJob()
        {
            return new CopyJob(this);
        }


        /// <summary>Creates the folder in a build process.</summary>
        /// <param name="path">Folder path.</param>
        /// <param name="meta">Metadata.</param>
        public void Build(string path, ExecutionMetadata meta)
        {
            Directory.CreateDirectory(path + @"\" + Name);
            meta.Target.AppendLog("nuub::build.createdfolder".Localize("Created folder \"$(1)\".").Replace("$(1)", Name));

            foreach(CopyJob i in Jobs)
            {
                i.Build(path + @"\" + Name, meta);
            }

            foreach(Folder i in Folders)
            {
                i.Build(path + @"\" + Name, meta);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Checks if a folder is unique.</summary>
        private void _CheckUnique()
        {
            IDbCommand cmd = null;

            if(ID == null)
            {
                if(Parent == null)
                {
                    cmd = Repository._Connection.CreateCommand("SELECT COUNT(*) FROM FOLDERS WHERE KPACKAGE = :package AND KPARENT IS NULL AND Lower(NAME) = :name");
                    cmd.AddParameter(":package", Package.ID);
                }
                else
                {
                    cmd = Repository._Connection.CreateCommand("SELECT COUNT(*) FROM FOLDERS WHERE KPARENT = :folder AND Lower(NAME) = :name");
                    cmd.AddParameter(":folder", Parent.ID);
                }
            }
            else
            {
                if(Parent == null)
                {
                    cmd = Repository._Connection.CreateCommand("SELECT COUNT(*) FROM FOLDERS WHERE KPACKAGE = :package AND KPARENT IS NULL AND ID != :id AND Lower(NAME) = :name");
                    cmd.AddParameter(":package", Package.ID);
                    cmd.AddParameter(":id", ID);
                }
                else
                {
                    cmd = Repository._Connection.CreateCommand("SELECT COUNT(*) FROM FOLDERS WHERE KPARENT = :folder AND ID != :id AND Lower(NAME) = :name");
                    cmd.AddParameter(":folder", Parent.ID);
                    cmd.AddParameter(":id", ID);
                }
            }
            cmd.AddParameter(":name", Name.ToLower());

            long rval = (long) cmd.ExecuteScalar();
            Disposal.Recycle(cmd);

            if(rval != 0) { throw new InvalidOperationException("nuub::folder.exists".Localize("Folder already exists.")); }
        }


        /// <summary>Gets a unique name.</summary>
        /// <param name="name">Base name.</param>
        /// <returns>Unique name.</returns>
        private string _GetUniqueName(string name)
        {
            string rval = null;
            int n = 0;

            while(true)
            {
                rval = name;
                if(++n > 1) { rval += " (" + n.ToString() + ")"; }

                IDbCommand cmd = null;

                if(Parent == null)
                {
                    cmd = Repository._Connection.CreateCommand("SELECT COUNT(*) FROM FOLDERS WHERE KPACKAGE = :package AND KPARENT IS NULL AND NAME = :name");
                    cmd.AddParameter(":package", Package.ID);
                }
                else
                {
                    cmd = Repository._Connection.CreateCommand("SELECT COUNT(*) FROM FOLDERS WHERE KPARENT = :folder AND NAME = :name");
                    cmd.AddParameter(":folder", Parent.ID);
                }
                cmd.AddParameter(":name", rval);

                if(((long) cmd.ExecuteScalar()) == 0)
                {
                    Disposal.Recycle(cmd);
                    break;
                }
                Disposal.Dispose(cmd);
            }

            return rval;
        }


        /// <summary>Updates package and cascades change to child folders.</summary>
        /// <param name="package">Package</param>
        private void _UpdatePackage(Package package)
        {
            Package = package;
            foreach(Folder i in Folders) { i._UpdatePackage(package); }
        }


        /// <summary>Gets if this folder is a child folder of a given folder.</summary>
        /// <param name="folder">Folder.</param>
        /// <returns>Returns TRUE if the folder is a child, otherwise returns FALSE.</returns>
        private bool _IsChildOf(Folder folder)
        {
            Folder cur = this;

            while(cur != null)
            {
                if(cur.ID == folder.ID) { return true; }

                cur = cur.Parent;
            }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the folder ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }


        /// <summary>Gets the package for this folder.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            IDbCommand cmd = Repository._Connection.CreateCommand("DELETE FROM FOLDERS WHERE ID = :id");
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            if(Parent == null)
            {
                Package.Folders._Items.Remove(ID);
            }
            else
            {
                Parent.Folders._Items.Remove(ID);
            }
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            _CheckUnique();

            IDbCommand cmd = null;
            bool register = false;

            if(ID == null)
            {
                ID = StringOp.Unique();
                register = true;

                if(Parent == null)
                {
                    cmd = Repository._Connection.CreateCommand("INSERT INTO FOLDERS (ID, KPACKAGE, KPARENT, NAME, DESCRIPTION) VALUES (:id, :package, NULL, :name, :descr)");
                    cmd.AddParameter(":id", ID);
                    cmd.AddParameter(":package", Package.ID);
                }
                else
                {
                    cmd = Repository._Connection.CreateCommand("INSERT INTO FOLDERS (ID, KPACKAGE, KPARENT, NAME, DESCRIPTION) VALUES (:id, NULL, :folder, :name, :descr)");
                    cmd.AddParameter(":id", ID);
                    cmd.AddParameter(":folder", Parent.ID);
                }
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
            }
            else
            {
                cmd = Repository._Connection.CreateCommand("UPDATE FOLDERS SET NAME = :name, DESCRIPTION = :descr WHERE ID = :id");
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":id", ID);
            }
            cmd.ExecuteNonQuery();

            Disposal.Recycle(cmd);

            if(register)
            {
                if(Parent == null)
                {
                    Package.Folders._Items.Add(ID, this);
                }
                else
                {
                    Parent.Folders._Items.Add(ID, this);
                }
            }
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(target is Package)
            {
                return new Folder((Package) target, null, this);
            }
            else if(target is Folder)
            {
                return new Folder(((Folder) target).Package, (Folder) target, this);
            }
            else { return null; }
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(target is Package)
            {
                if((((Package) target).ID == Package.ID) && (Parent == null)) { return null; }

                ((Parent == null) ? Package.Folders : Parent.Folders)._Items.Remove(ID);

                _UpdatePackage((Package) target);
                Parent = null;

                Name = _GetUniqueName(Name);
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE FOLDERS SET KPACKAGE = :package, KPARENT = NULL, NAME = :name WHERE ID = :id");
                cmd.AddParameter(":package", Package.ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":id", ID);
                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Package.Folders._Items.Add(ID, this);

                return this;
            }
            else if(target is Folder)
            {
                if(((Folder) target)._IsChildOf(this)) { return null; }

                ((Parent == null) ? Package.Folders : Parent.Folders)._Items.Remove(ID);

                Parent = ((Folder) target);
                _UpdatePackage(Parent.Package);

                Name = _GetUniqueName(Name);
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE FOLDERS SET KPACKAGE = NULL, KPARENT = :folder, NAME = :name WHERE ID = :id");
                cmd.AddParameter(":folder", Parent.ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":id", ID);
                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Parent.Folders._Items.Add(ID, this);

                return this;
            }

            return null;
        }
    }
}
