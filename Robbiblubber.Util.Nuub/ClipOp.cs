﻿using Robbiblubber.Util.Localization.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements clipboard helper functionality.</summary>
    internal static class __ClipOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static properties                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the drop target node.</summary>
        internal static TreeNode _DropTarget { get; set; }


        /// <summary>Gets or sets the dragged node.</summary>
        internal static TreeNode _DragNode { get; set; }


        /// <summary>Gets or sets the current drag key state.</summary>
        internal static int _DragState { get; set; }
        
        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static properties                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if a drop operation is allowed.</summary>
        internal static bool _DropAllowed
        {
            get
            {
                if((_DragNode == null) || (_DropTarget == null) || (!(_DragNode.Tag is IItem))) { return false; }

                return _DragNode._IsAssignableTo(_DropTarget);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal extension methods                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Determines if a tree node is dragable.</summary>
        /// <param name="node">Tree node.</param>
        /// <returns>Returns TRUE if the node may be dragged, otherwise returns FALSE.</returns>
        internal static bool _IsDragable(this TreeNode node)
        {
            if(node.Tag == null) return false;
            if(node.Tag is Repository) return false;
            if(node.Tag is Solution) return false;

            return true;
        }


        /// <summary>Returns if a tree node is assignable to another tree node.</summary>
        /// <param name="node">Tree node.</param>
        /// <param name="targetNode">Target tree node.</param>
        /// <returns>Returns TRUE if the item can be assigned to the target, otherwise returns false.</returns>
        internal static bool _IsAssignableTo(this TreeNode node, TreeNode targetNode)
        {
            return _IsAssignableTo((IItem) node.Tag, (IItem) targetNode.Tag);
        }


        /// <summary>Returns if an item is assignable to another item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="target">Target item.</param>
        /// <returns>Returns TRUE if the item can be assigned to the target, otherwise returns false.</returns>
        internal static bool _IsAssignableTo(this IItem item, IItem target)
        {
            if(target is Solution) { return (item is Package); }
            if(target is Package) { return ((item is Folder) || (item is IDependency)); }
            if(target is Folder) { return ((item is Folder) || (item is CopyJob)); }

            return false;
        }


        /// <summary>Gets the item class name for an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Class name.</returns>
        internal static string _GetClassName(this IItem item)
        {
            if(item is Repository) return "nuub::item.repository".Localize("repository");
            if(item is Solution) return "nuub::item.solution".Localize("solution");
            if(item is Package) return "nuub::item.package".Localize("package");
            if(item is Folder) return "nuub::item.folder".Localize("folder");
            if(item is CopyJob) return "nuub::item.copyjob".Localize("copy job");
            if(item is PkgDependency) return "nuub::item.pkgdependency".Localize("package dependency");
            if(item is NuDependency) return "nuub::item.nudependency".Localize("NuGet dependency");
            if(item is FwDependency) return "nuub::item.fwdependency".Localize("framework dependency");

            return "unknown item";
        }
    }
}
