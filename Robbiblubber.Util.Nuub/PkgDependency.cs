﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a dependency.</summary>
    public sealed class PkgDependency: IItem, IDependency
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Target package ID.</summary>
        internal string _TargetID = null;

        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="re">Reader.</param>
        public PkgDependency(Package package, IDataReader re)
        {
            Package = package;

            ID = re.GetString(0);
            if(re.IsDBNull(1))
            {
                _TargetID = null;
            }
            else { _TargetID = re.GetString(1); }

            Name = re.GetString(2);
            Description = re.GetString(3);            
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        public PkgDependency(Package package)
        {
            Package = package;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";

            Save();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="template">Template.</param>
        public PkgDependency(Package package, PkgDependency template)
        {
            Package = package;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            _TargetID = template._TargetID;

            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }

        
        /// <summary>Gets the parent package for this dependency.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency target.</summary>
        public Package Target
        {
            get { return (Package) Repository._ObjectByID(_TargetID); }
            set
            {
                if(value == null)
                {
                    _TargetID = null;
                }
                else { _TargetID = value.ID; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            IDbCommand cmd = Repository._Connection.CreateCommand("DELETE FROM PKGDEPENDENCIES WHERE ID = :id");
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Package.PackageDependencies._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            if(ID == null)
            {
                ID = StringOp.Unique();

                IDbCommand cmd = Repository._Connection.CreateCommand("INSERT INTO PKGDEPENDENCIES (ID, KPACKAGE, KTARGET, NAME, DESCRIPTION) VALUES (:id, :package, :tar, :name, :descr)");
                cmd.AddParameter(":id", ID);
                cmd.AddParameter(":package", Package.ID);
                cmd.AddParameter(":tar", _TargetID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Package.PackageDependencies._Items.Add(ID, this);
            }
            else
            {
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE PKGDEPENDENCIES SET KTARGET = :tar, NAME = :name, DESCRIPTION = :descr WHERE ID = :id");
                cmd.AddParameter(":tar", _TargetID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":id", ID);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);
            }
        }

        
        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Package)) return null;

            return new PkgDependency((Package) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is Package)) return null;
            if(((Package) target).ID == Package.ID) return null;

            Package.PackageDependencies._Items.Remove(ID);

            Package = ((Package) target);
            IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE PKGDEPENDENCIES SET KPACKAGE = :package WHERE ID = :id");
            cmd.AddParameter(":package", Package.ID);
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Disposal.Recycle(cmd);

            Package.PackageDependencies._Items.Add(ID, this);

            return this;
        }
    }
}
