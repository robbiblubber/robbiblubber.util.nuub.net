﻿using System;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This enumeration defines log severities.</summary>
    public enum LogSeverity: int
    {
        /// <summary>Information log entry.</summary>
        INFORMATION = 0,
        /// <summary>Warning log entry.</summary>
        WARNING = 1,
        /// <summary>Error log entry.</summary>
        ERROR = 2
    }
}
