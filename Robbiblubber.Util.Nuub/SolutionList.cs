﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Collections;
using Robbiblubber.Data;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class provides a list of solutions for a repository.</summary>
    public sealed class SolutionList: IImmutableList<Solution>, IEnumerable<Solution>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        internal Dictionary<string, Solution> _Items = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="repository">Repository.</param>
        internal SolutionList(Repository repository)
        {
            Repository = repository;

            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a solution item for a given name.</summary>
        /// <param name="name">Name.</param>
        /// <returns>Solution.</returns>
        public Solution this[string name]
        {
            get
            {
                foreach(Solution i in _Items.Values)
                {
                    if(i.Name == name) return i;
                }

                return null;
            }
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a soulution with a given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Solution.</returns>
        public Solution ForID(string id)
        {
            return _Items[id];
        }


        /// <summary>Refreshes this instance.</summary>
        public void Refresh()
        {
            _Items = new Dictionary<string, Solution>();

            IDbCommand cmd = Repository._Connection.CreateCommand("SELECT ID, NAME, DESCRIPTION, VERSION, AUTHORS, OWNERS, COPYRIGHT, LICENSE_URL, PROJECT_URL, REP_URL, REP_TYPE, TAGS, ICON_URL, RELEASE_NOTES FROM SOLUTIONS");
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                _Items.Add(re.GetString(0), new Solution(Repository, re));
            }
            Disposal.Recycle(re, cmd);
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="name">Item name.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public bool Contains(string name)
        {
            foreach(Solution i in _Items.Values)
            {
                if(i.Name == name) return true;
            }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IReadOnlyList<Solution>                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        Solution IReadOnlyList<Solution>.this[int i]
        {
            get { return _Items.Values.ElementAt(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList<Solution>                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of items in this collection.</summary>
        public int Count
        {
            get { return _Items.Count; }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        bool IImmutableList<Solution>.Contains(Solution item)
        {
            return _Items.Values.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        int IImmutableList<Solution>.IndexOf(Solution item)
        {
            return _Items.Values.GetIndex(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<Solution>                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator</returns>
        IEnumerator<Solution> IEnumerable<Solution>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
