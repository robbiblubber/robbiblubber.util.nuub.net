﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements the folder control.</summary>
    public sealed partial class FolderControl: UserControl, IControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tree node.</summary>
        private TreeNode _Node;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FolderControl()
        {
            InitializeComponent();
        }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IControl                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set
            {
                _Node = value;
                if(value == null) return;

                Reload();
            }
        }


        /// <summary>Gets if the contents have been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_TextName.Text != ((Folder) _Node.Tag).Name) return true;
                if(_TextDescription.Text != ((Folder) _Node.Tag).Description) return true;

                return false;
            }
        }


        /// <summary>Saves the contents.</summary>
        /// <param name="name">New name.</param>
        /// <returns>Returns TRUE if the object has been saved, FALSE if an error has occured.</returns>
        public bool Save(string name = null)
        {
            if(name != null) { _TextName.Text = name; }

            ((Folder) _Node.Tag).Name = _TextName.Text;
            ((Folder) _Node.Tag).Description = _TextDescription.Text;

            try
            {
                ((Folder) _Node.Tag).Save();

                if(name == null) { _Node.Text = ((Folder) _Node.Tag).Name; }
            }
            catch(Exception) { MessageBox.Show("nuub::udiag.save.uniqerr".Localize("The object could not be saved. The name already exists in this context."), "nuub::udiag.save.caption".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error); }

            _ButtonSave.Enabled = false;

            return true;
        }


        /// <summary>Refreshes the contents.</summary>
        public void Reload()
        {
            _TextName.Text = ((Folder) _Node.Tag).Name;
            _TextDescription.Text = ((Folder) _Node.Tag).Description;

            _Node.Text = ((Folder) _Node.Tag).Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Resize.</summary>
        private void _Resize(object sender, EventArgs e)
        {
            _TextName.Width = _TextDescription.Width = (Width - 77);
        }


        /// <summary>Text changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            _ButtonSave.Enabled = Modified;
        }


        /// <summary>Button "Save" click.</summary>
        private void _ButtonSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>Button "Reload" click.</summary>
        private void _ButtonReload_Click(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
