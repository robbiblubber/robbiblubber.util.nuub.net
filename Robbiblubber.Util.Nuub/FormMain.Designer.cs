﻿namespace Robbiblubber.Util.Nuub
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this._MenuMain = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRepository = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCreateRepository = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuOpenRepository = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAdd = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddSolution = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddPackage = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddFolder = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddCopyJob = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddPkgDependency = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddNuDependency = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAddFwDependency = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuItems = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaveItem = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuReloadItem = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditLabel = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuBuildOrDeploy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuBuild = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDeploy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuToolsBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStart = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStop = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebugSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugUpload = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._SplitMain = new System.Windows.Forms.SplitContainer();
            this._TreeMain = new System.Windows.Forms.TreeView();
            this._CmenuTree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuTreeBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuAdd = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddSolution = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddPackage = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddFolder = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddCopyJob = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddPkgDependency = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddNuDependency = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuAddFwDependency = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuTreeBlank1 = new System.Windows.Forms.ToolStripSeparator();
            this._CmenuBuild = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuDeploy = new System.Windows.Forms.ToolStripMenuItem();
            this._IlistTree = new System.Windows.Forms.ImageList(this.components);
            this._FwDependencyCtrl = new Robbiblubber.Util.Nuub.FwDependencyControl();
            this._NuDependencyCtrl = new Robbiblubber.Util.Nuub.NuDependencyControl();
            this._PkgDependencyCtrl = new Robbiblubber.Util.Nuub.PkgDependencyControl();
            this._CopyJobCtrl = new Robbiblubber.Util.Nuub.CopyJobControl();
            this._FolderCtrl = new Robbiblubber.Util.Nuub.FolderControl();
            this._PackageCtrl = new Robbiblubber.Util.Nuub.PackageControl();
            this._SolutionCtrl = new Robbiblubber.Util.Nuub.SolutionControl();
            this._RepositoryCtrl = new Robbiblubber.Util.Nuub.RepositoryControl();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._CmenuDrop = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._CmenuDropCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._CmenuDropMove = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPurgeOutput = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._SplitMain)).BeginInit();
            this._SplitMain.Panel1.SuspendLayout();
            this._SplitMain.Panel2.SuspendLayout();
            this._SplitMain.SuspendLayout();
            this._CmenuTree.SuspendLayout();
            this._CmenuDrop.SuspendLayout();
            this.SuspendLayout();
            // 
            // _MenuMain
            // 
            this._MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this._MenuEdit,
            this._MenuBuildOrDeploy,
            this._MenuTools,
            this.helpToolStripMenuItem});
            this._MenuMain.Location = new System.Drawing.Point(0, 0);
            this._MenuMain.Name = "_MenuMain";
            this._MenuMain.Padding = new System.Windows.Forms.Padding(7, 3, 0, 3);
            this._MenuMain.Size = new System.Drawing.Size(1047, 25);
            this._MenuMain.TabIndex = 0;
            this._MenuMain.Text = "menuStrip1";
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuRepository,
            this._MenuAdd,
            this._MenuFileBlank1,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.Size = new System.Drawing.Size(37, 19);
            this._MenuFile.Tag = "nuub::menu.file";
            this._MenuFile.Text = "&File";
            this._MenuFile.DropDownClosed += new System.EventHandler(this._MenuFile_DropDownClosed);
            this._MenuFile.DropDownOpening += new System.EventHandler(this._MenuFile_DropDownOpening);
            // 
            // _MenuRepository
            // 
            this._MenuRepository.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuCreateRepository,
            this._MenuOpenRepository});
            this._MenuRepository.Name = "_MenuRepository";
            this._MenuRepository.Size = new System.Drawing.Size(180, 22);
            this._MenuRepository.Tag = "nuub::menu.repository";
            this._MenuRepository.Text = "&Repository";
            // 
            // _MenuCreateRepository
            // 
            this._MenuCreateRepository.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCreateRepository.Image")));
            this._MenuCreateRepository.Name = "_MenuCreateRepository";
            this._MenuCreateRepository.Size = new System.Drawing.Size(203, 22);
            this._MenuCreateRepository.Tag = "nuub::menu.createrepository";
            this._MenuCreateRepository.Text = "Create &New Repository...";
            this._MenuCreateRepository.Click += new System.EventHandler(this._MenuCreateRepository_Click);
            // 
            // _MenuOpenRepository
            // 
            this._MenuOpenRepository.Image = ((System.Drawing.Image)(resources.GetObject("_MenuOpenRepository.Image")));
            this._MenuOpenRepository.Name = "_MenuOpenRepository";
            this._MenuOpenRepository.Size = new System.Drawing.Size(203, 22);
            this._MenuOpenRepository.Tag = "nuub::menu.openrepository";
            this._MenuOpenRepository.Text = "&Open Repository...";
            this._MenuOpenRepository.Click += new System.EventHandler(this._MenuOpenRepository_Click);
            // 
            // _MenuAdd
            // 
            this._MenuAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuAddSolution,
            this._MenuAddPackage,
            this._MenuAddFolder,
            this._MenuAddCopyJob,
            this._MenuAddPkgDependency,
            this._MenuAddNuDependency,
            this._MenuAddFwDependency});
            this._MenuAdd.Name = "_MenuAdd";
            this._MenuAdd.Size = new System.Drawing.Size(180, 22);
            this._MenuAdd.Tag = "nuub::menu.add";
            this._MenuAdd.Text = "&Add";
            this._MenuAdd.DropDownClosed += new System.EventHandler(this._MenuAdd_DropDownClosed);
            this._MenuAdd.DropDownOpening += new System.EventHandler(this._MenuAdd_DropDownOpening);
            // 
            // _MenuAddSolution
            // 
            this._MenuAddSolution.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddSolution.Image")));
            this._MenuAddSolution.Name = "_MenuAddSolution";
            this._MenuAddSolution.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this._MenuAddSolution.Size = new System.Drawing.Size(270, 22);
            this._MenuAddSolution.Tag = "nuub::menu.addsolution";
            this._MenuAddSolution.Text = "&Solution";
            this._MenuAddSolution.Click += new System.EventHandler(this._MenuAddSolution_Click);
            // 
            // _MenuAddPackage
            // 
            this._MenuAddPackage.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddPackage.Image")));
            this._MenuAddPackage.Name = "_MenuAddPackage";
            this._MenuAddPackage.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.P)));
            this._MenuAddPackage.Size = new System.Drawing.Size(270, 22);
            this._MenuAddPackage.Tag = "nuub::menu.addpackage";
            this._MenuAddPackage.Text = "&Package";
            this._MenuAddPackage.Click += new System.EventHandler(this._MenuAddPackage_Click);
            // 
            // _MenuAddFolder
            // 
            this._MenuAddFolder.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddFolder.Image")));
            this._MenuAddFolder.Name = "_MenuAddFolder";
            this._MenuAddFolder.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.F)));
            this._MenuAddFolder.Size = new System.Drawing.Size(270, 22);
            this._MenuAddFolder.Tag = "nuub::menu.addfolder";
            this._MenuAddFolder.Text = "&Folder";
            this._MenuAddFolder.Click += new System.EventHandler(this._MenuAddFolder_Click);
            // 
            // _MenuAddCopyJob
            // 
            this._MenuAddCopyJob.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddCopyJob.Image")));
            this._MenuAddCopyJob.Name = "_MenuAddCopyJob";
            this._MenuAddCopyJob.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.J)));
            this._MenuAddCopyJob.Size = new System.Drawing.Size(270, 22);
            this._MenuAddCopyJob.Tag = "nuub::menu.addjob";
            this._MenuAddCopyJob.Text = "&Copy Job";
            this._MenuAddCopyJob.Click += new System.EventHandler(this._MenuAddCopyJob_Click);
            // 
            // _MenuAddPkgDependency
            // 
            this._MenuAddPkgDependency.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddPkgDependency.Image")));
            this._MenuAddPkgDependency.Name = "_MenuAddPkgDependency";
            this._MenuAddPkgDependency.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this._MenuAddPkgDependency.Size = new System.Drawing.Size(270, 22);
            this._MenuAddPkgDependency.Tag = "nuub::menu.addpkgdep";
            this._MenuAddPkgDependency.Text = "Package Dependency";
            this._MenuAddPkgDependency.Click += new System.EventHandler(this._MenuAddPkgDependency_Click);
            // 
            // _MenuAddNuDependency
            // 
            this._MenuAddNuDependency.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddNuDependency.Image")));
            this._MenuAddNuDependency.Name = "_MenuAddNuDependency";
            this._MenuAddNuDependency.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.N)));
            this._MenuAddNuDependency.Size = new System.Drawing.Size(270, 22);
            this._MenuAddNuDependency.Tag = "nuub::menu.addnudep";
            this._MenuAddNuDependency.Text = "NuGet Dependency";
            this._MenuAddNuDependency.Click += new System.EventHandler(this._MenuAddNuDependency_Click);
            // 
            // _MenuAddFwDependency
            // 
            this._MenuAddFwDependency.Image = ((System.Drawing.Image)(resources.GetObject("_MenuAddFwDependency.Image")));
            this._MenuAddFwDependency.Name = "_MenuAddFwDependency";
            this._MenuAddFwDependency.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.W)));
            this._MenuAddFwDependency.Size = new System.Drawing.Size(270, 22);
            this._MenuAddFwDependency.Tag = "nuub::menu.addfwdep";
            this._MenuAddFwDependency.Text = "Framework Dependency";
            this._MenuAddFwDependency.Click += new System.EventHandler(this._MenuAddFwDependency_Click);
            // 
            // _MenuFileBlank1
            // 
            this._MenuFileBlank1.Name = "_MenuFileBlank1";
            this._MenuFileBlank1.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExit.Image")));
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this._MenuExit.Size = new System.Drawing.Size(180, 22);
            this._MenuExit.Tag = "nuub::menu.exit";
            this._MenuExit.Text = "&Exit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuUndo,
            this._MenuEditBlank0,
            this._MenuCut,
            this._MenuCopy,
            this._MenuPaste,
            this._MenuDelete,
            this._MenuItems});
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 19);
            this._MenuEdit.Text = "&Edit";
            this._MenuEdit.DropDownClosed += new System.EventHandler(this._MenuEdit_DropDownClosed);
            this._MenuEdit.DropDownOpening += new System.EventHandler(this._MenuEdit_DropDownOpening);
            // 
            // _MenuUndo
            // 
            this._MenuUndo.Image = ((System.Drawing.Image)(resources.GetObject("_MenuUndo.Image")));
            this._MenuUndo.Name = "_MenuUndo";
            this._MenuUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this._MenuUndo.Size = new System.Drawing.Size(180, 22);
            this._MenuUndo.Tag = "nuub::menu.undo";
            this._MenuUndo.Text = "&Undo";
            this._MenuUndo.Click += new System.EventHandler(this._MenuUndo_Click);
            // 
            // _MenuEditBlank0
            // 
            this._MenuEditBlank0.Name = "_MenuEditBlank0";
            this._MenuEditBlank0.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuCut
            // 
            this._MenuCut.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCut.Image")));
            this._MenuCut.Name = "_MenuCut";
            this._MenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._MenuCut.Size = new System.Drawing.Size(180, 22);
            this._MenuCut.Tag = "nuub::menu.cut";
            this._MenuCut.Text = "Cu&t";
            this._MenuCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _MenuCopy
            // 
            this._MenuCopy.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCopy.Image")));
            this._MenuCopy.Name = "_MenuCopy";
            this._MenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._MenuCopy.Size = new System.Drawing.Size(180, 22);
            this._MenuCopy.Tag = "nuub::menu.copy";
            this._MenuCopy.Text = "&Copy";
            this._MenuCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _MenuPaste
            // 
            this._MenuPaste.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPaste.Image")));
            this._MenuPaste.Name = "_MenuPaste";
            this._MenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._MenuPaste.Size = new System.Drawing.Size(180, 22);
            this._MenuPaste.Tag = "nuub::menu.paste";
            this._MenuPaste.Text = "&Paste";
            this._MenuPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // _MenuDelete
            // 
            this._MenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDelete.Image")));
            this._MenuDelete.Name = "_MenuDelete";
            this._MenuDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this._MenuDelete.Size = new System.Drawing.Size(180, 22);
            this._MenuDelete.Tag = "nuub::menu.delete";
            this._MenuDelete.Text = "&Delete...";
            this._MenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _MenuItems
            // 
            this._MenuItems.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuSaveItem,
            this._MenuReloadItem,
            this._MenuEditLabel});
            this._MenuItems.Name = "_MenuItems";
            this._MenuItems.Size = new System.Drawing.Size(180, 22);
            this._MenuItems.Text = "Items";
            // 
            // _MenuSaveItem
            // 
            this._MenuSaveItem.Name = "_MenuSaveItem";
            this._MenuSaveItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._MenuSaveItem.Size = new System.Drawing.Size(151, 22);
            this._MenuSaveItem.Text = "Save";
            this._MenuSaveItem.Click += new System.EventHandler(this._MenuSaveItem_Click);
            // 
            // _MenuReloadItem
            // 
            this._MenuReloadItem.Name = "_MenuReloadItem";
            this._MenuReloadItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this._MenuReloadItem.Size = new System.Drawing.Size(151, 22);
            this._MenuReloadItem.Text = "Reload";
            this._MenuReloadItem.Click += new System.EventHandler(this._MenuReloadItem_Click);
            // 
            // _MenuEditLabel
            // 
            this._MenuEditLabel.Name = "_MenuEditLabel";
            this._MenuEditLabel.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this._MenuEditLabel.Size = new System.Drawing.Size(151, 22);
            this._MenuEditLabel.Text = "Edit";
            this._MenuEditLabel.Click += new System.EventHandler(this._MenuEditLabel_Click);
            // 
            // _MenuBuildOrDeploy
            // 
            this._MenuBuildOrDeploy.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuBuild,
            this._MenuDeploy});
            this._MenuBuildOrDeploy.Name = "_MenuBuildOrDeploy";
            this._MenuBuildOrDeploy.Size = new System.Drawing.Size(46, 19);
            this._MenuBuildOrDeploy.Text = "&Build";
            this._MenuBuildOrDeploy.DropDownClosed += new System.EventHandler(this._MenuBuildOrDeploy_DropDownClosed);
            this._MenuBuildOrDeploy.DropDownOpening += new System.EventHandler(this._MenuBuildOrDeploy_DropDownOpening);
            // 
            // _MenuBuild
            // 
            this._MenuBuild.Image = ((System.Drawing.Image)(resources.GetObject("_MenuBuild.Image")));
            this._MenuBuild.Name = "_MenuBuild";
            this._MenuBuild.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this._MenuBuild.Size = new System.Drawing.Size(180, 22);
            this._MenuBuild.Text = "&Build...";
            this._MenuBuild.Click += new System.EventHandler(this._MenuBuild_Click);
            // 
            // _MenuDeploy
            // 
            this._MenuDeploy.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDeploy.Image")));
            this._MenuDeploy.Name = "_MenuDeploy";
            this._MenuDeploy.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this._MenuDeploy.Size = new System.Drawing.Size(180, 22);
            this._MenuDeploy.Text = "&Deploy...";
            this._MenuDeploy.Click += new System.EventHandler(this._MenuDeploy_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuRefresh,
            this._MenuPurgeOutput,
            this._MenuToolsBlank0,
            this._MenuDebug,
            this._MenuSettings});
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(47, 19);
            this._MenuTools.Text = "&Tools";
            // 
            // _MenuRefresh
            // 
            this._MenuRefresh.Image = ((System.Drawing.Image)(resources.GetObject("_MenuRefresh.Image")));
            this._MenuRefresh.Name = "_MenuRefresh";
            this._MenuRefresh.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this._MenuRefresh.Size = new System.Drawing.Size(182, 22);
            this._MenuRefresh.Text = "&Refresh";
            this._MenuRefresh.Click += new System.EventHandler(this._MenuRefresh_Click);
            // 
            // _MenuToolsBlank0
            // 
            this._MenuToolsBlank0.Name = "_MenuToolsBlank0";
            this._MenuToolsBlank0.Size = new System.Drawing.Size(179, 6);
            // 
            // _MenuDebug
            // 
            this._MenuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebugStart,
            this._MenuDebugStop,
            this._MenuDebugBlank0,
            this._MenuDebugSave,
            this._MenuDebugView,
            this._MenuDebugUpload});
            this._MenuDebug.Name = "_MenuDebug";
            this._MenuDebug.Size = new System.Drawing.Size(182, 22);
            this._MenuDebug.Tag = "debugx::menu.debug";
            this._MenuDebug.Text = "Debug";
            this._MenuDebug.DropDownClosed += new System.EventHandler(this._MenuDebug_DropDownClosed);
            this._MenuDebug.DropDownOpening += new System.EventHandler(this._MenuDebug_DropDownOpening);
            // 
            // _MenuDebugStart
            // 
            this._MenuDebugStart.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStart.Image")));
            this._MenuDebugStart.Name = "_MenuDebugStart";
            this._MenuDebugStart.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStart.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStart.Tag = "debugx::menu.startlogging";
            this._MenuDebugStart.Text = "Start &Logging";
            this._MenuDebugStart.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugStop
            // 
            this._MenuDebugStop.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStop.Image")));
            this._MenuDebugStop.Name = "_MenuDebugStop";
            this._MenuDebugStop.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStop.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStop.Tag = "debugx::menu.stoplogging";
            this._MenuDebugStop.Text = "Stop &Logging";
            this._MenuDebugStop.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugBlank0
            // 
            this._MenuDebugBlank0.Name = "_MenuDebugBlank0";
            this._MenuDebugBlank0.Size = new System.Drawing.Size(219, 6);
            // 
            // _MenuDebugSave
            // 
            this._MenuDebugSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugSave.Image")));
            this._MenuDebugSave.Name = "_MenuDebugSave";
            this._MenuDebugSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this._MenuDebugSave.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugSave.Tag = "debugx::menu.savedump";
            this._MenuDebugSave.Text = "&Save Dump...";
            this._MenuDebugSave.Click += new System.EventHandler(this._MenuDebugSave_Click);
            // 
            // _MenuDebugView
            // 
            this._MenuDebugView.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugView.Image")));
            this._MenuDebugView.Name = "_MenuDebugView";
            this._MenuDebugView.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.V)));
            this._MenuDebugView.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugView.Tag = "debugx::menu.viewdump";
            this._MenuDebugView.Text = "&View Dump...";
            this._MenuDebugView.Click += new System.EventHandler(this._MenuDebugView_Click);
            // 
            // _MenuDebugUpload
            // 
            this._MenuDebugUpload.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugUpload.Image")));
            this._MenuDebugUpload.Name = "_MenuDebugUpload";
            this._MenuDebugUpload.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.U)));
            this._MenuDebugUpload.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugUpload.Tag = "debugx::menu.uploaddump";
            this._MenuDebugUpload.Text = "&Upload Dump...";
            this._MenuDebugUpload.Click += new System.EventHandler(this._MenuDebugUpload_Click);
            // 
            // _MenuSettings
            // 
            this._MenuSettings.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSettings.Image")));
            this._MenuSettings.Name = "_MenuSettings";
            this._MenuSettings.Size = new System.Drawing.Size(182, 22);
            this._MenuSettings.Text = "Settings...";
            this._MenuSettings.Click += new System.EventHandler(this._MenuSettings_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showHelpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 19);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // showHelpToolStripMenuItem
            // 
            this.showHelpToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("showHelpToolStripMenuItem.Image")));
            this.showHelpToolStripMenuItem.Name = "showHelpToolStripMenuItem";
            this.showHelpToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.showHelpToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.showHelpToolStripMenuItem.Text = "Show &Help...";
            this.showHelpToolStripMenuItem.Click += new System.EventHandler(this.showHelpToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // _SplitMain
            // 
            this._SplitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SplitMain.Location = new System.Drawing.Point(0, 25);
            this._SplitMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SplitMain.Name = "_SplitMain";
            // 
            // _SplitMain.Panel1
            // 
            this._SplitMain.Panel1.Controls.Add(this._TreeMain);
            // 
            // _SplitMain.Panel2
            // 
            this._SplitMain.Panel2.Controls.Add(this._FwDependencyCtrl);
            this._SplitMain.Panel2.Controls.Add(this._NuDependencyCtrl);
            this._SplitMain.Panel2.Controls.Add(this._PkgDependencyCtrl);
            this._SplitMain.Panel2.Controls.Add(this._CopyJobCtrl);
            this._SplitMain.Panel2.Controls.Add(this._FolderCtrl);
            this._SplitMain.Panel2.Controls.Add(this._PackageCtrl);
            this._SplitMain.Panel2.Controls.Add(this._SolutionCtrl);
            this._SplitMain.Panel2.Controls.Add(this._RepositoryCtrl);
            this._SplitMain.Size = new System.Drawing.Size(1047, 495);
            this._SplitMain.SplitterDistance = 348;
            this._SplitMain.SplitterWidth = 5;
            this._SplitMain.TabIndex = 1;
            // 
            // _TreeMain
            // 
            this._TreeMain.AllowDrop = true;
            this._TreeMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TreeMain.ContextMenuStrip = this._CmenuTree;
            this._TreeMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this._TreeMain.HideSelection = false;
            this._TreeMain.ImageIndex = 0;
            this._TreeMain.ImageList = this._IlistTree;
            this._TreeMain.Location = new System.Drawing.Point(0, 0);
            this._TreeMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._TreeMain.Name = "_TreeMain";
            this._TreeMain.PathSeparator = "/";
            this._TreeMain.SelectedImageIndex = 0;
            this._TreeMain.Size = new System.Drawing.Size(348, 495);
            this._TreeMain.TabIndex = 0;
            this._TreeMain.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this._TreeMain_AfterLabelEdit);
            this._TreeMain.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._TreeMain_AfterSelect);
            this._TreeMain.DragDrop += new System.Windows.Forms.DragEventHandler(this._TreeMain_DragDrop);
            this._TreeMain.DragOver += new System.Windows.Forms.DragEventHandler(this._TreeMain_DragOver);
            this._TreeMain.KeyDown += new System.Windows.Forms.KeyEventHandler(this._TreeMain_KeyDown);
            this._TreeMain.MouseDown += new System.Windows.Forms.MouseEventHandler(this._TreeMain_MouseDown);
            this._TreeMain.MouseMove += new System.Windows.Forms.MouseEventHandler(this._TreeMain_MouseMove);
            // 
            // _CmenuTree
            // 
            this._CmenuTree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuCut,
            this._CmenuCopy,
            this._CmenuPaste,
            this._CmenuDelete,
            this._CmenuTreeBlank0,
            this._CmenuAdd,
            this._CmenuTreeBlank1,
            this._CmenuBuild,
            this._CmenuDeploy});
            this._CmenuTree.Name = "_CmenuTree";
            this._CmenuTree.Size = new System.Drawing.Size(121, 170);
            this._CmenuTree.Opening += new System.ComponentModel.CancelEventHandler(this._CmenuTree_Opening);
            // 
            // _CmenuCut
            // 
            this._CmenuCut.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuCut.Image")));
            this._CmenuCut.Name = "_CmenuCut";
            this._CmenuCut.Size = new System.Drawing.Size(120, 22);
            this._CmenuCut.Tag = "nuub::menu.cut";
            this._CmenuCut.Text = "Cut";
            this._CmenuCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _CmenuCopy
            // 
            this._CmenuCopy.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuCopy.Image")));
            this._CmenuCopy.Name = "_CmenuCopy";
            this._CmenuCopy.Size = new System.Drawing.Size(120, 22);
            this._CmenuCopy.Tag = "nuub::menu.copy";
            this._CmenuCopy.Text = "Copy";
            this._CmenuCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _CmenuPaste
            // 
            this._CmenuPaste.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuPaste.Image")));
            this._CmenuPaste.Name = "_CmenuPaste";
            this._CmenuPaste.Size = new System.Drawing.Size(120, 22);
            this._CmenuPaste.Tag = "nuub::menu.paste";
            this._CmenuPaste.Text = "Paste";
            this._CmenuPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // _CmenuDelete
            // 
            this._CmenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDelete.Image")));
            this._CmenuDelete.Name = "_CmenuDelete";
            this._CmenuDelete.Size = new System.Drawing.Size(120, 22);
            this._CmenuDelete.Tag = "nuub::menu.delete";
            this._CmenuDelete.Text = "Delete...";
            this._CmenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _CmenuTreeBlank0
            // 
            this._CmenuTreeBlank0.Name = "_CmenuTreeBlank0";
            this._CmenuTreeBlank0.Size = new System.Drawing.Size(117, 6);
            // 
            // _CmenuAdd
            // 
            this._CmenuAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuAddSolution,
            this._CmenuAddPackage,
            this._CmenuAddFolder,
            this._CmenuAddCopyJob,
            this._CmenuAddPkgDependency,
            this._CmenuAddNuDependency,
            this._CmenuAddFwDependency});
            this._CmenuAdd.Name = "_CmenuAdd";
            this._CmenuAdd.Size = new System.Drawing.Size(120, 22);
            this._CmenuAdd.Tag = "nuub::menu.add";
            this._CmenuAdd.Text = "Add";
            this._CmenuAdd.DropDownClosed += new System.EventHandler(this._MenuAdd_DropDownClosed);
            this._CmenuAdd.DropDownOpening += new System.EventHandler(this._MenuAdd_DropDownOpening);
            // 
            // _CmenuAddSolution
            // 
            this._CmenuAddSolution.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddSolution.Image")));
            this._CmenuAddSolution.Name = "_CmenuAddSolution";
            this._CmenuAddSolution.Size = new System.Drawing.Size(202, 22);
            this._CmenuAddSolution.Tag = "nuub::menu.addsolution";
            this._CmenuAddSolution.Text = "Solution";
            this._CmenuAddSolution.Click += new System.EventHandler(this._MenuAddSolution_Click);
            // 
            // _CmenuAddPackage
            // 
            this._CmenuAddPackage.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddPackage.Image")));
            this._CmenuAddPackage.Name = "_CmenuAddPackage";
            this._CmenuAddPackage.Size = new System.Drawing.Size(202, 22);
            this._CmenuAddPackage.Tag = "nuub::menu.addpackage";
            this._CmenuAddPackage.Text = "Package";
            this._CmenuAddPackage.Click += new System.EventHandler(this._MenuAddPackage_Click);
            // 
            // _CmenuAddFolder
            // 
            this._CmenuAddFolder.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddFolder.Image")));
            this._CmenuAddFolder.Name = "_CmenuAddFolder";
            this._CmenuAddFolder.Size = new System.Drawing.Size(202, 22);
            this._CmenuAddFolder.Tag = "nuub::menu.addfolder";
            this._CmenuAddFolder.Text = "Folder";
            this._CmenuAddFolder.Click += new System.EventHandler(this._MenuAddFolder_Click);
            // 
            // _CmenuAddCopyJob
            // 
            this._CmenuAddCopyJob.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddCopyJob.Image")));
            this._CmenuAddCopyJob.Name = "_CmenuAddCopyJob";
            this._CmenuAddCopyJob.Size = new System.Drawing.Size(202, 22);
            this._CmenuAddCopyJob.Tag = "nuub::menu.addjob";
            this._CmenuAddCopyJob.Text = "Copy Job";
            this._CmenuAddCopyJob.Click += new System.EventHandler(this._MenuAddCopyJob_Click);
            // 
            // _CmenuAddPkgDependency
            // 
            this._CmenuAddPkgDependency.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddPkgDependency.Image")));
            this._CmenuAddPkgDependency.Name = "_CmenuAddPkgDependency";
            this._CmenuAddPkgDependency.Size = new System.Drawing.Size(202, 22);
            this._CmenuAddPkgDependency.Tag = "nuub::menu.addpkgdep";
            this._CmenuAddPkgDependency.Text = "Package Dependency";
            this._CmenuAddPkgDependency.Click += new System.EventHandler(this._MenuAddPkgDependency_Click);
            // 
            // _CmenuAddNuDependency
            // 
            this._CmenuAddNuDependency.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddNuDependency.Image")));
            this._CmenuAddNuDependency.Name = "_CmenuAddNuDependency";
            this._CmenuAddNuDependency.Size = new System.Drawing.Size(202, 22);
            this._CmenuAddNuDependency.Tag = "nuub::menu.addnudep";
            this._CmenuAddNuDependency.Text = "NuGet Dependency";
            this._CmenuAddNuDependency.Click += new System.EventHandler(this._MenuAddNuDependency_Click);
            // 
            // _CmenuAddFwDependency
            // 
            this._CmenuAddFwDependency.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuAddFwDependency.Image")));
            this._CmenuAddFwDependency.Name = "_CmenuAddFwDependency";
            this._CmenuAddFwDependency.Size = new System.Drawing.Size(202, 22);
            this._CmenuAddFwDependency.Tag = "nuub::menu.addfwdep";
            this._CmenuAddFwDependency.Text = "Framework Dependency";
            this._CmenuAddFwDependency.Click += new System.EventHandler(this._MenuAddFwDependency_Click);
            // 
            // _CmenuTreeBlank1
            // 
            this._CmenuTreeBlank1.Name = "_CmenuTreeBlank1";
            this._CmenuTreeBlank1.Size = new System.Drawing.Size(117, 6);
            // 
            // _CmenuBuild
            // 
            this._CmenuBuild.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuBuild.Image")));
            this._CmenuBuild.Name = "_CmenuBuild";
            this._CmenuBuild.Size = new System.Drawing.Size(120, 22);
            this._CmenuBuild.Tag = "nuub::menu.build";
            this._CmenuBuild.Text = "Build...";
            this._CmenuBuild.Click += new System.EventHandler(this._MenuBuild_Click);
            // 
            // _CmenuDeploy
            // 
            this._CmenuDeploy.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDeploy.Image")));
            this._CmenuDeploy.Name = "_CmenuDeploy";
            this._CmenuDeploy.Size = new System.Drawing.Size(120, 22);
            this._CmenuDeploy.Tag = "nuub::menu.deploy";
            this._CmenuDeploy.Text = "Deploy...";
            this._CmenuDeploy.Click += new System.EventHandler(this._MenuDeploy_Click);
            // 
            // _IlistTree
            // 
            this._IlistTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistTree.ImageStream")));
            this._IlistTree.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistTree.Images.SetKeyName(0, "img::repository");
            this._IlistTree.Images.SetKeyName(1, "img::solution");
            this._IlistTree.Images.SetKeyName(2, "img::package");
            this._IlistTree.Images.SetKeyName(3, "img::folder");
            this._IlistTree.Images.SetKeyName(4, "img::job");
            this._IlistTree.Images.SetKeyName(5, "img::pkgdep");
            this._IlistTree.Images.SetKeyName(6, "img::nudep");
            this._IlistTree.Images.SetKeyName(7, "img::fwdep");
            // 
            // _FwDependencyCtrl
            // 
            this._FwDependencyCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._FwDependencyCtrl.Location = new System.Drawing.Point(286, 121);
            this._FwDependencyCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._FwDependencyCtrl.Name = "_FwDependencyCtrl";
            this._FwDependencyCtrl.Node = null;
            this._FwDependencyCtrl.Size = new System.Drawing.Size(48, 96);
            this._FwDependencyCtrl.TabIndex = 7;
            this._FwDependencyCtrl.Visible = false;
            // 
            // _NuDependencyCtrl
            // 
            this._NuDependencyCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._NuDependencyCtrl.Location = new System.Drawing.Point(232, 121);
            this._NuDependencyCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._NuDependencyCtrl.Name = "_NuDependencyCtrl";
            this._NuDependencyCtrl.Node = null;
            this._NuDependencyCtrl.Size = new System.Drawing.Size(48, 96);
            this._NuDependencyCtrl.TabIndex = 6;
            this._NuDependencyCtrl.Visible = false;
            // 
            // _PkgDependencyCtrl
            // 
            this._PkgDependencyCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._PkgDependencyCtrl.Location = new System.Drawing.Point(286, 17);
            this._PkgDependencyCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PkgDependencyCtrl.Name = "_PkgDependencyCtrl";
            this._PkgDependencyCtrl.Node = null;
            this._PkgDependencyCtrl.Size = new System.Drawing.Size(48, 96);
            this._PkgDependencyCtrl.TabIndex = 5;
            this._PkgDependencyCtrl.Visible = false;
            // 
            // _CopyJobCtrl
            // 
            this._CopyJobCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._CopyJobCtrl.Location = new System.Drawing.Point(232, 17);
            this._CopyJobCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._CopyJobCtrl.Name = "_CopyJobCtrl";
            this._CopyJobCtrl.Node = null;
            this._CopyJobCtrl.Size = new System.Drawing.Size(48, 96);
            this._CopyJobCtrl.TabIndex = 4;
            this._CopyJobCtrl.Visible = false;
            // 
            // _FolderCtrl
            // 
            this._FolderCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._FolderCtrl.Location = new System.Drawing.Point(178, 17);
            this._FolderCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._FolderCtrl.Name = "_FolderCtrl";
            this._FolderCtrl.Node = null;
            this._FolderCtrl.Size = new System.Drawing.Size(48, 96);
            this._FolderCtrl.TabIndex = 3;
            this._FolderCtrl.Visible = false;
            // 
            // _PackageCtrl
            // 
            this._PackageCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._PackageCtrl.Location = new System.Drawing.Point(124, 17);
            this._PackageCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._PackageCtrl.Name = "_PackageCtrl";
            this._PackageCtrl.Node = null;
            this._PackageCtrl.Size = new System.Drawing.Size(48, 96);
            this._PackageCtrl.TabIndex = 2;
            this._PackageCtrl.Visible = false;
            // 
            // _SolutionCtrl
            // 
            this._SolutionCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._SolutionCtrl.Location = new System.Drawing.Point(70, 17);
            this._SolutionCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._SolutionCtrl.Name = "_SolutionCtrl";
            this._SolutionCtrl.Node = null;
            this._SolutionCtrl.Size = new System.Drawing.Size(48, 96);
            this._SolutionCtrl.TabIndex = 1;
            this._SolutionCtrl.Visible = false;
            // 
            // _RepositoryCtrl
            // 
            this._RepositoryCtrl.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._RepositoryCtrl.Location = new System.Drawing.Point(16, 17);
            this._RepositoryCtrl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._RepositoryCtrl.Name = "_RepositoryCtrl";
            this._RepositoryCtrl.Node = null;
            this._RepositoryCtrl.Size = new System.Drawing.Size(48, 96);
            this._RepositoryCtrl.TabIndex = 0;
            this._RepositoryCtrl.Visible = false;
            // 
            // _CmenuDrop
            // 
            this._CmenuDrop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._CmenuDropCopy,
            this._CmenuDropMove});
            this._CmenuDrop.Name = "_CmenuDrop";
            this._CmenuDrop.Size = new System.Drawing.Size(105, 48);
            // 
            // _CmenuDropCopy
            // 
            this._CmenuDropCopy.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDropCopy.Image")));
            this._CmenuDropCopy.Name = "_CmenuDropCopy";
            this._CmenuDropCopy.Size = new System.Drawing.Size(104, 22);
            this._CmenuDropCopy.Tag = "nuub::menu.copy";
            this._CmenuDropCopy.Text = "Copy";
            this._CmenuDropCopy.Click += new System.EventHandler(this._CmenuDropCopy_Click);
            // 
            // _CmenuDropMove
            // 
            this._CmenuDropMove.Image = ((System.Drawing.Image)(resources.GetObject("_CmenuDropMove.Image")));
            this._CmenuDropMove.Name = "_CmenuDropMove";
            this._CmenuDropMove.Size = new System.Drawing.Size(104, 22);
            this._CmenuDropMove.Tag = "nuub::menu.move";
            this._CmenuDropMove.Text = "Move";
            this._CmenuDropMove.Click += new System.EventHandler(this._CmenuDropMove_Click);
            // 
            // _MenuPurgeOutput
            // 
            this._MenuPurgeOutput.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPurgeOutput.Image")));
            this._MenuPurgeOutput.Name = "_MenuPurgeOutput";
            this._MenuPurgeOutput.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F10)));
            this._MenuPurgeOutput.Size = new System.Drawing.Size(253, 22);
            this._MenuPurgeOutput.Text = "&Purge Output Drirectory";
            this._MenuPurgeOutput.Click += new System.EventHandler(this._MenuPurgeOutput_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 520);
            this.Controls.Add(this._SplitMain);
            this.Controls.Add(this._MenuMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuMain;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMain";
            this.Text = "Nuub Builder";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this._MenuMain.ResumeLayout(false);
            this._MenuMain.PerformLayout();
            this._SplitMain.Panel1.ResumeLayout(false);
            this._SplitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._SplitMain)).EndInit();
            this._SplitMain.ResumeLayout(false);
            this._CmenuTree.ResumeLayout(false);
            this._CmenuDrop.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _MenuMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.SplitContainer _SplitMain;
        private System.Windows.Forms.TreeView _TreeMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuAdd;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddSolution;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddPackage;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddFolder;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddCopyJob;
        private System.Windows.Forms.ToolStripSeparator _MenuFileBlank1;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _MenuRepository;
        private System.Windows.Forms.ToolStripMenuItem _MenuCreateRepository;
        private System.Windows.Forms.ToolStripMenuItem _MenuOpenRepository;
        private System.Windows.Forms.ImageList _IlistTree;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddPkgDependency;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddNuDependency;
        private System.Windows.Forms.ToolTip _ToolTip;
        private RepositoryControl _RepositoryCtrl;
        private SolutionControl _SolutionCtrl;
        private PackageControl _PackageCtrl;
        private FolderControl _FolderCtrl;
        private CopyJobControl _CopyJobCtrl;
        private PkgDependencyControl _PkgDependencyCtrl;
        private System.Windows.Forms.ToolStripMenuItem _MenuAddFwDependency;
        private NuDependencyControl _NuDependencyCtrl;
        private FwDependencyControl _FwDependencyCtrl;
        private System.Windows.Forms.ToolStripMenuItem _MenuUndo;
        private System.Windows.Forms.ToolStripSeparator _MenuEditBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuCut;
        private System.Windows.Forms.ToolStripMenuItem _MenuCopy;
        private System.Windows.Forms.ToolStripMenuItem _MenuPaste;
        private System.Windows.Forms.ToolStripMenuItem _MenuDelete;
        private System.Windows.Forms.ToolStripMenuItem _MenuBuildOrDeploy;
        private System.Windows.Forms.ToolStripMenuItem _MenuBuild;
        private System.Windows.Forms.ToolStripMenuItem _MenuDeploy;
        private System.Windows.Forms.ContextMenuStrip _CmenuDrop;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDropCopy;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDropMove;
        private System.Windows.Forms.ToolStripMenuItem _MenuRefresh;
        private System.Windows.Forms.ToolStripSeparator _MenuToolsBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebug;
        private System.Windows.Forms.ToolStripMenuItem _MenuSettings;
        private System.Windows.Forms.ContextMenuStrip _CmenuTree;
        private System.Windows.Forms.ToolStripMenuItem _CmenuCut;
        private System.Windows.Forms.ToolStripMenuItem _CmenuCopy;
        private System.Windows.Forms.ToolStripMenuItem _CmenuPaste;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDelete;
        private System.Windows.Forms.ToolStripSeparator _CmenuTreeBlank0;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAdd;
        private System.Windows.Forms.ToolStripSeparator _CmenuTreeBlank1;
        private System.Windows.Forms.ToolStripMenuItem _CmenuBuild;
        private System.Windows.Forms.ToolStripMenuItem _CmenuDeploy;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddSolution;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddPackage;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddFolder;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddCopyJob;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddPkgDependency;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddNuDependency;
        private System.Windows.Forms.ToolStripMenuItem _CmenuAddFwDependency;
        private System.Windows.Forms.ToolStripMenuItem _MenuItems;
        private System.Windows.Forms.ToolStripMenuItem _MenuSaveItem;
        private System.Windows.Forms.ToolStripMenuItem _MenuReloadItem;
        private System.Windows.Forms.ToolStripMenuItem _MenuEditLabel;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStop;
        private System.Windows.Forms.ToolStripSeparator _MenuDebugBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugView;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugUpload;
        private System.Windows.Forms.ToolStripMenuItem _MenuPurgeOutput;
    }
}

