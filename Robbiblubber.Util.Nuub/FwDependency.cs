﻿using System;
using System.Data;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a framework dependency.</summary>
    public sealed class FwDependency: IItem, IDependency
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="re">Reader.</param>
        public FwDependency(Package package, IDataReader re)
        {
            Package = package;

            ID = re.GetString(0);
            Name = re.GetString(1);
            Description = re.GetString(2);
            AssemblyName = re.GetString(3);
            TargetFramework = re.GetString(4);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        public FwDependency(Package package)
        {
            Package = package;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";
            AssemblyName = "";
            TargetFramework = "";

            Save();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="template">Template.</param>
        public FwDependency(Package package, FwDependency template)
        {
            Package = package;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            AssemblyName = template.AssemblyName;
            TargetFramework = template.TargetFramework;

            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }


        /// <summary>Gets the parent package for this dependency.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency assembly name.</summary>
        public string AssemblyName
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency target framework.</summary>
        public string TargetFramework
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            IDbCommand cmd = Repository._Connection.CreateCommand("DELETE FROM FWDEPENDENCIES WHERE ID = :id");
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Package.FrameworkDependencies._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            if(ID == null)
            {
                ID = StringOp.Unique();

                IDbCommand cmd = Repository._Connection.CreateCommand("INSERT INTO FWDEPENDENCIES (ID, KPACKAGE, NAME, DESCRIPTION, ASMNAME, TARGETFW) VALUES (:id, :package, :name, :descr, :asm, :tar)");
                cmd.AddParameter(":id", ID);
                cmd.AddParameter(":package", Package.ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":asm", AssemblyName);
                cmd.AddParameter(":tar", TargetFramework);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Package.FrameworkDependencies._Items.Add(ID, this);
            }
            else
            {
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE FWDEPENDENCIES SET NAME = :name, DESCRIPTION = :descr, ASMNAME = :asm, TARGETFW = :tar WHERE ID = :id");
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":asm", AssemblyName);
                cmd.AddParameter(":tar", TargetFramework);
                cmd.AddParameter(":id", ID);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);
            }
        }

        
        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Package)) return null;

            return new FwDependency((Package) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is Package)) return null;
            if(((Package) target).ID == Package.ID) return null;

            Package.FrameworkDependencies._Items.Remove(ID);

            Package = ((Package) target);
            IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE FWDEPENDENCIES SET KPACKAGE = :package WHERE ID = :id");
            cmd.AddParameter(":package", Package.ID);
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Disposal.Recycle(cmd);

            Package.FrameworkDependencies._Items.Add(ID, this);

            return this;
        }
    }
}
