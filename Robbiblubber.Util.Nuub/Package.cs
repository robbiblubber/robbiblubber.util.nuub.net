﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Threading;
using System.Text;
using System.Diagnostics;

using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a Nuub package.</summary>
    public sealed class Package: IItem, IExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="solution">Solution.</param>
        /// <param name="re">Reader.</param>
        public Package(Solution solution, IDataReader re)
        {
            Solution = solution;

            ID = re.GetString(0);
            Name = re.GetString(1);
            Description = re.GetString(2);

            Version = (string.IsNullOrWhiteSpace(re.GetString(3)) ? null : new Version(re.GetString(3)));

            Authors = re.GetString(4);
            Owners = re.GetString(5);
            Copyright = re.GetString(6);
            LicenseURL = re.GetString(7);

            ProjectURL = re.GetString(8);
            RepositoryURL = re.GetString(9);
            RepositoryType = re.GetString(10);

            Tags = re.GetString(11);
            IconURL = re.GetString(12);
            ReleaseNotes = re.GetString(13);

            Folders = new FolderList(this, null);
            PackageDependencies = new PkgDependencyList(this);
            NuGetDependencies = new NuDependencyList(this);
            FrameworkDependencies = new FwDependencyList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="solution">Solution</param>
        public Package(Solution solution)
        {
            Solution = solution;

            ID = null;
            Name = Repository._GetUniqueName("nuub::udiag.unnamed".Localize("Unnamed"), "PACKAGES");
            Description = "";

            Version = null;

            Authors = Owners = Copyright = LicenseURL = "";

            ProjectURL = RepositoryURL = RepositoryType = "";

            Tags = IconURL = ReleaseNotes = "";

            Save();

            Folders = new FolderList(this, null);
            PackageDependencies = new PkgDependencyList(this);
            NuGetDependencies = new NuDependencyList(this);
            FrameworkDependencies = new FwDependencyList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="template">Template package.</param>
        /// <param name="solution">Repository</param>
        public Package(Solution solution, Package template)
        {
            Solution = solution;

            ID = null;
            Name = Repository._GetUniqueName(template.Name, "PACKAGES");
            Description = template.Description;

            Version = ((template.Version == null) ? null : new Version(template.Version.ToVersionString()));

            Authors = template.Authors;
            Owners = template.Owners;
            Copyright = template.Copyright;
            LicenseURL = template.LicenseURL;

            ProjectURL = template.ProjectURL;
            RepositoryURL = template.RepositoryURL;
            RepositoryType = template.RepositoryType;

            Tags = template.Tags;
            IconURL = template.IconURL;
            ReleaseNotes = template.ReleaseNotes;

            Save();

            Folders = new FolderList(this, null);
            PackageDependencies = new PkgDependencyList(this);
            NuGetDependencies = new NuDependencyList(this);
            FrameworkDependencies = new FwDependencyList(this);

            foreach(Folder i in template.Folders) { new Folder(this, null, i); }
            foreach(PkgDependency i in template.PackageDependencies) { new PkgDependency(this, i); }
            foreach(NuDependency i in template.NuGetDependencies) { new NuDependency(this, i); }
            foreach(FwDependency i in template.FrameworkDependencies) { new FwDependency(this, i); }
        }
        
        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the solution version.</summary>
        public Version Version
        {
            get; set;
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Authors
        {
            get; set;
        }


        /// <summary>Gets or sets the solution owners.</summary>
        public string Owners
        {
            get; set;
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Copyright
        {
            get; set;
        }


        /// <summary>Gets or sets the solution license URL.</summary>
        public string LicenseURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution project URL.</summary>
        public string ProjectURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution icon URL.</summary>
        public string IconURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution tags.</summary>
        public string Tags
        {
            get; set;
        }


        /// <summary>Gets or sets the solution release notes.</summary>
        public string ReleaseNotes
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository URL.</summary>
        public string RepositoryURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository type.</summary>
        public string RepositoryType
        {
            get; set;
        }


        /// <summary>Gets the parent solution.</summary>
        public Solution Solution
        {
            get; set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Solution.Repository; }
        }


        /// <summary>Gets the folders for this package.</summary>
        public FolderList Folders
        {
            get; private set;
        }


        /// <summary>Gets the package dependencies for this package.</summary>
        public PkgDependencyList PackageDependencies
        {
            get; private set;
        }


        /// <summary>Gets the NuGet dependencies for this package.</summary>
        public NuDependencyList NuGetDependencies
        {
            get; private set;
        }


        /// <summary>Gets the framework dependencies for this package.</summary>
        public FwDependencyList FrameworkDependencies
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a new folder to the package.</summary>
        /// <returns>Folder.</returns>
        public Folder AddFolder()
        {
            return new Folder(this);
        }


        /// <summary>Adds a new package dependency to the package.</summary>
        /// <returns>Folder.</returns>
        public PkgDependency AddPackageDependency()
        {
            return new PkgDependency(this);
        }


        /// <summary>Adds a new package dependency to the package.</summary>
        /// <returns>Folder.</returns>
        public NuDependency AddNuGetDependency()
        {
            return new NuDependency(this);
        }


        /// <summary>Adds a new package dependency to the package.</summary>
        /// <returns>Folder.</returns>
        public FwDependency AddFrameworkDependency()
        {
            return new FwDependency(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a NuGet package.</summary>
        /// <param name="meta"></param>
        private void _CreatePackage(ExecutionMetadata meta)
        {
            try
            {
                Process p = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = Program._Settings.NuGetExecutable,
                        Arguments = "pack " + meta.OutputDirectory + @"\temp\" + Name + ".nuspec -OutputDirectory " + meta.OutputDirectory,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true
                    }
                };

                p.Start();

                while(!p.StandardOutput.EndOfStream)
                {
                    string l = p.StandardOutput.ReadLine();

                    if(l.ToLower().StartsWith("error"))
                    {
                        meta.Target.AppendLog(LogSeverity.ERROR, l);
                    }
                    else if(l.ToLower().StartsWith("warning"))
                    {
                        meta.Target.AppendLog(LogSeverity.WARNING, l);
                    }
                    else { meta.Target.AppendLog(LogSeverity.INFORMATION, l); }
                }

                while(!p.StandardError.EndOfStream)
                {
                    meta.Target.AppendLog(LogSeverity.ERROR, p.StandardError.ReadLine());
                }
            }
            catch(Exception ex)
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::build.packfailed".Localize("Failed to build package: $(1)").Replace("$(1)", ex.Message));
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the solution ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the solution name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the solution description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            IDbCommand cmd = Repository._Connection.CreateCommand("DELETE FROM PACKAGES WHERE ID = :id");
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Solution.Packages._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            if(ID == null)
            {
                ID = StringOp.Unique();

                IDbCommand cmd = Repository._Connection.CreateCommand("INSERT INTO PACKAGES (ID, KSOLUTION, NAME, DESCRIPTION, VERSION, AUTHORS, OWNERS, COPYRIGHT, LICENSE_URL, PROJECT_URL, REP_URL, REP_TYPE, TAGS, ICON_URL, RELEASE_NOTES) VALUES (:id, :solution, :name, :descr, :v, :authors, :owners, :copyright, :licurl, :prjurl, :repurl, :reptype, :tags, :icon, :relnotes)");
                cmd.AddParameter(":id", ID);
                cmd.AddParameter(":solution", Solution.ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":v", ((Version == null) ? "" : Version.ToVersionString()));

                cmd.AddParameter(":authors", Authors);
                cmd.AddParameter(":owners", Owners);
                cmd.AddParameter(":copyright", Copyright);
                cmd.AddParameter(":licurl", LicenseURL);

                cmd.AddParameter(":prjurl", ProjectURL);
                cmd.AddParameter(":repurl", RepositoryURL);
                cmd.AddParameter(":reptype", RepositoryType);

                cmd.AddParameter(":tags", Tags);
                cmd.AddParameter(":icon", IconURL);
                cmd.AddParameter(":relnotes", ReleaseNotes);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Solution.Packages._Items.Add(ID, this);
            }
            else
            {
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE PACKAGES SET KSOLUTION = :solution, NAME = :name, DESCRIPTION = :descr, VERSION = :v, AUTHORS = :authors, OWNERS = :owners, COPYRIGHT = :copyright, LICENSE_URL = :licurl, PROJECT_URL = :prjurl, REP_URL = :repurl, REP_TYPE = :reptype, TAGS = :tags, ICON_URL = :icon, RELEASE_NOTES = :relnotes WHERE ID = :id");
                cmd.AddParameter(":solution", Solution.ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);

                cmd.AddParameter(":v", ((Version == null) ? "" : Version.ToVersionString()));

                cmd.AddParameter(":authors", Authors);
                cmd.AddParameter(":owners", Owners);
                cmd.AddParameter(":copyright", Copyright);
                cmd.AddParameter(":licurl", LicenseURL);

                cmd.AddParameter(":prjurl", ProjectURL);
                cmd.AddParameter(":repurl", RepositoryURL);
                cmd.AddParameter(":reptype", RepositoryType);

                cmd.AddParameter(":tags", Tags);
                cmd.AddParameter(":icon", IconURL);
                cmd.AddParameter(":relnotes", ReleaseNotes);

                cmd.AddParameter(":id", ID);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);
            }
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Solution)) return null;

            return new Package((Solution) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is Solution)) return null;
            if(((Solution) target).ID == Solution.ID) return null;

            Solution.Packages._Items.Remove(ID);

            Solution = ((Solution) target);
            IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE PACKAGES SET KSOLUTION = :solution WHERE ID = :id");
            cmd.AddParameter(":solution", Solution.ID);
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Disposal.Recycle(cmd);

            Solution.Packages._Items.Add(ID, this);

            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExecutable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Builds this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Build(ExecutionMetadata meta)
        {
            meta.Target.AppendLog("nuub::build.buildingpackage".Localize("Building package \"$(1)\".").Replace("$(1)", Name));

            try
            {
                if(Directory.Exists(meta.OutputDirectory + @"\temp"))
                {
                    Directory.Delete(meta.OutputDirectory + @"\temp", true);
                }

                Thread.Sleep(60);
                Directory.CreateDirectory(meta.OutputDirectory + @"\temp");

                foreach(Folder i in Folders)
                {
                    i.Build(meta.OutputDirectory + @"\temp", meta);
                }
            }
            catch(Exception) { meta.Target.AppendLog(LogSeverity.WARNING, "nuub::build.tmpfailed".Localize("Failed to create temporary directory.")); }

            try
            { 
                XmlTextWriter xml = new XmlTextWriter(meta.OutputDirectory + @"\temp\" + Name + ".nuspec", Encoding.UTF8);
                xml.Formatting = Formatting.Indented;
                xml.Indentation = 3;

                xml.WriteStartDocument();

                xml.WriteStartElement("package");
                xml.WriteStartElement("metadata");

                xml.WriteStartElement("id");
                xml.WriteString(Name);
                xml.WriteEndElement();

                xml.WriteStartElement("version");
                xml.WriteString((Version == null) ? Solution.Version.ToVersionString() : Version.ToVersionString());
                xml.WriteEndElement();

                xml.WriteStartElement("authors");
                xml.WriteString(string.IsNullOrWhiteSpace(Authors) ? Solution.Authors : Authors);
                xml.WriteEndElement();

                xml.WriteStartElement("owners");
                xml.WriteString(string.IsNullOrWhiteSpace(Owners) ? Solution.Owners : Owners);
                xml.WriteEndElement();

                xml.WriteStartElement("requireLicenseAcceptance");
                xml.WriteString("false");
                xml.WriteEndElement();

                xml.WriteStartElement("licenseUrl");
                xml.WriteString(string.IsNullOrWhiteSpace(LicenseURL) ? Solution.LicenseURL : LicenseURL);
                xml.WriteEndElement();

                xml.WriteStartElement("projectUrl");
                xml.WriteString(string.IsNullOrWhiteSpace(ProjectURL) ? Solution.ProjectURL : ProjectURL);
                xml.WriteEndElement();

                xml.WriteStartElement("iconUrl");
                xml.WriteString(string.IsNullOrWhiteSpace(IconURL) ? Solution.IconURL : IconURL);
                xml.WriteEndElement();

                xml.WriteStartElement("description");
                xml.WriteString(string.IsNullOrWhiteSpace(Description) ? Solution.Description : Description);
                xml.WriteEndElement();

                xml.WriteStartElement("releaseNotes");
                xml.WriteString(string.IsNullOrWhiteSpace(ReleaseNotes) ? Solution.ReleaseNotes : ReleaseNotes);
                xml.WriteEndElement();

                xml.WriteStartElement("copyright");
                xml.WriteString(string.IsNullOrWhiteSpace(Copyright) ? Solution.Copyright : Copyright);
                xml.WriteEndElement();

                xml.WriteStartElement("tags");
                xml.WriteString(string.IsNullOrWhiteSpace(Tags) ? Solution.Tags : Tags);
                xml.WriteEndElement();

                xml.WriteStartElement("repository");
                xml.WriteAttributeString("type", string.IsNullOrWhiteSpace(RepositoryType) ? Solution.RepositoryType : RepositoryType);
                xml.WriteAttributeString("url", string.IsNullOrWhiteSpace(RepositoryURL) ? Solution.RepositoryURL : RepositoryURL);
                xml.WriteEndElement();

                if((PackageDependencies.Count + NuGetDependencies.Count) > 0)
                {
                    xml.WriteStartElement("dependencies");

                    foreach(PkgDependency i in PackageDependencies)
                    {
                        xml.WriteStartElement("dependency");
                        xml.WriteAttributeString("id", i.Target.Name);
                        xml.WriteAttributeString("version", (i.Target.Version == null) ? i.Target.Solution.Version.ToVersionString() : i.Target.Version.ToVersionString());
                        xml.WriteEndElement();

                        meta.Target.AppendLog("nuub::build.addeddep".Localize("Added dependency for \"$(1)\".").Replace("$(1)", i.Target.Name));
                    }

                    foreach(NuDependency i in NuGetDependencies)
                    {
                        xml.WriteStartElement("dependency");
                        xml.WriteAttributeString("id", i.PackageID);
                        xml.WriteAttributeString("version", i.PackageVersion);
                        xml.WriteEndElement();

                        meta.Target.AppendLog("nuub::build.addeddep".Localize("Added dependency for \"$(1)\".").Replace("$(1)", i.PackageID));
                    }

                    xml.WriteEndElement();
                }

                if(FrameworkDependencies.Count > 0)
                {
                    xml.WriteStartElement("frameworkAssemblies");

                    foreach(FwDependency i in FrameworkDependencies)
                    {
                        xml.WriteStartElement("frameworkAssembly");
                        xml.WriteAttributeString("assemblyName", i.AssemblyName);
                        if(!string.IsNullOrWhiteSpace(i.TargetFramework)) { xml.WriteAttributeString("targetFramework", i.TargetFramework); }
                        xml.WriteEndElement();

                        meta.Target.AppendLog("nuub::build.addeddep".Localize("Added dependency for \"$(1)\".").Replace("$(1)", i.AssemblyName));
                    }

                    xml.WriteEndElement();
                }

                xml.WriteEndElement();
                xml.WriteEndElement();
                xml.WriteEndDocument();

                xml.Close();
            }
            catch(Exception ex)
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::build.filefailed".Localize("Failed to create .nuspec file: $(1)").Replace("$(1)", ex.Message));
                return;
            }
            
            meta.Target.AppendLog("nuub::build.createdfile".Localize("Created file \"$(1)\".").Replace("$(1)", Name + ".nuspec"));

            _CreatePackage(meta);

            try
            {
                Directory.Delete(meta.OutputDirectory + @"\temp", true);
            }
            catch(Exception) { meta.Target.AppendLog(LogSeverity.WARNING, "nuub::build.rtmpfailed".Localize("Failed to remove temporary directory.")); }
            meta.Target.AppendLog("");
        }


        /// <summary>Deploys this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Deploy(ExecutionMetadata meta)
        {
            if(!File.Exists(meta.OutputDirectory + @"\" + Name + "." + Version + ".nupkg"))
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::deploy.filefailed".Localize("Unable to deploy. Package \"$(1)\" was not found.").Replace("$(1)", Name + "." + Version + ".nupkg"));
                return;
            }

            meta.Target.AppendLog("nuub::deploy.deployinggpackage".Localize("Deploying package \"$(1)\".").Replace("$(1)", Name));

            try
            {
                Process p = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = Program._Settings.NuGetExecutable,
                        Arguments = "push " + meta.OutputDirectory + @"\" + Name + "." + Version + ".nupkg " + Program._Settings.APIKey + " -Source https://api.nuget.org/v3/index.json",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                p.Start();

                while(!p.StandardOutput.EndOfStream)
                {
                    string l = p.StandardOutput.ReadLine();

                    if(l.ToLower().StartsWith("error"))
                    {
                        meta.Target.AppendLog(LogSeverity.ERROR, l);
                    }
                    else if(l.ToLower().StartsWith("warning"))
                    {
                        meta.Target.AppendLog(LogSeverity.WARNING, l);
                    }
                    else { meta.Target.AppendLog(LogSeverity.INFORMATION, l); }
                }
            }
            catch(Exception ex)
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::deploy.packfailed".Localize("Failed to deploy package: $(1)").Replace("$(1)", ex.Message));
            }
        }
    }
}
