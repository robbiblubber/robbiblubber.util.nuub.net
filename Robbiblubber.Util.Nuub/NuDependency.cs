﻿using System;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.Localization.Controls;
using Robbiblubber.Data;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a NuGet dependency.</summary>
    public sealed class NuDependency: IItem, IDependency
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="re">Reader.</param>
        public NuDependency(Package package, IDataReader re)
        {
            Package = package;

            ID = re.GetString(0);
            Name = re.GetString(1);
            Description = re.GetString(2);
            PackageID = re.GetString(3);
            PackageVersion = re.GetString(4);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        public NuDependency(Package package)
        {
            Package = package;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";
            PackageID = "";
            PackageVersion = "";

            Save();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="template">Template.</param>
        public NuDependency(Package package, NuDependency template)
        {
            Package = package;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            PackageID = template.PackageID;
            PackageVersion = template.PackageVersion;

            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency package ID.</summary>
        public string PackageID
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency package version.</summary>
        public string PackageVersion
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }


        /// <summary>Gets the parent package for this dependency.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            IDbCommand cmd = Repository._Connection.CreateCommand("DELETE FROM NUDEPENDENCIES WHERE ID = :id");
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Package.NuGetDependencies._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            if(ID == null)
            {
                ID = StringOp.Unique();

                IDbCommand cmd = Repository._Connection.CreateCommand("INSERT INTO NUDEPENDENCIES (ID, KPACKAGE, NAME, DESCRIPTION, PKGID, PKGVERSION) VALUES (:id, :package, :name, :descr, :pkgid, :pkgv)");
                cmd.AddParameter(":id", ID);
                cmd.AddParameter(":package", Package.ID);
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":pkgid", PackageID);
                cmd.AddParameter(":pkgv", PackageVersion);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);

                Package.NuGetDependencies._Items.Add(ID, this);
            }
            else
            {
                IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE NUDEPENDENCIES SET NAME = :name, DESCRIPTION = :descr, PKGID = :pkgid, PKGVERSION = :pkgv WHERE ID = :id");
                cmd.AddParameter(":name", Name);
                cmd.AddParameter(":descr", Description);
                cmd.AddParameter(":pkgid", PackageID);
                cmd.AddParameter(":pkgv", PackageVersion);
                cmd.AddParameter(":id", ID);

                cmd.ExecuteNonQuery();

                Disposal.Recycle(cmd);
            }
        }

        
        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Package)) return null;

            return new NuDependency((Package) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is Package)) return null;
            if(((Package) target).ID == Package.ID) return null;

            Package.NuGetDependencies._Items.Remove(ID);

            Package = ((Package) target);
            IDbCommand cmd = Repository._Connection.CreateCommand("UPDATE NUDEPENDENCIES SET KPACKAGE = :package WHERE ID = :id");
            cmd.AddParameter(":package", Package.ID);
            cmd.AddParameter(":id", ID);
            cmd.ExecuteNonQuery();

            Disposal.Recycle(cmd);

            Package.NuGetDependencies._Items.Add(ID, this);

            return this;
        }
    }
}
