﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements the repository control.</summary>
    public sealed partial class PkgDependencyControl: UserControl, IControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tree node.</summary>
        private TreeNode _Node;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public PkgDependencyControl()
        {
            InitializeComponent();
        }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IControl                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set
            {
                _Node = value;
                if(value == null) return;

                Reload();
            }
        }


        /// <summary>Gets if the contents have been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_TextName.Text != ((PkgDependency) _Node.Tag).Name) return true;
                if(_TextDescription.Text != ((PkgDependency) _Node.Tag).Description) return true;
                if(((__ComboItem) _CboTarget.SelectedItem).ID != ((PkgDependency) _Node.Tag)._TargetID) return true;

                return false;
            }
        }


        /// <summary>Saves the contents.</summary>
        /// <param name="name">New name.</param>
        /// <returns>Returns TRUE if the object has been saved, FALSE if an error has occured.</returns>
        public bool Save(string name = null)
        {
            if(name != null) { _TextName.Text = name; }

            ((PkgDependency) _Node.Tag).Name = _TextName.Text;
            ((PkgDependency) _Node.Tag).Description = _TextDescription.Text;
            ((PkgDependency) _Node.Tag).Target = (Package) ((__ComboItem) _CboTarget.SelectedItem).Item;

            ((PkgDependency) _Node.Tag).Save();

            if(name == null) { _Node.Text = ((PkgDependency) _Node.Tag).Name; }
            _ButtonSave.Enabled = false;

            return true;
        }


        /// <summary>Refreshes the contents.</summary>
        public void Reload()
        {
            _CboTarget.BeginUpdate();
            _CboTarget.Items.Clear();

            int sel = -1;
            __ComboItem m;

            foreach(Package i in ((PkgDependency) Node.Tag).Repository._GetPackages())
            {
                if(i.ID == ((PkgDependency) Node.Tag).Package.ID) continue;

                m = new __ComboItem(i);
                m.Index = _CboTarget.Items.Add(m);

                if(i.ID == ((PkgDependency) Node.Tag)._TargetID) { sel = m.Index; }
            }

            m = new __ComboItem(null);
            m.Index = _CboTarget.Items.Add(m);

            if(sel < 0) { sel = m.Index; }

            _CboTarget.SelectedIndex = sel;
            _CboTarget.EndUpdate();

            _TextName.Text = ((PkgDependency) _Node.Tag).Name;
            _TextDescription.Text = ((PkgDependency) _Node.Tag).Description;

            _Node.Text = ((PkgDependency) _Node.Tag).Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Resize.</summary>
        private void _Resize(object sender, EventArgs e)
        {
            _TextName.Width = _TextDescription.Width = _CboTarget.Width = (Width - 77);
        }


        /// <summary>Text changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            _ButtonSave.Enabled = Modified;
        }


        /// <summary>Button "Save" click.</summary>
        private void _ButtonSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>Button "Reload" click.</summary>
        private void _ButtonReload_Click(object sender, EventArgs e)
        {
            Reload();
        }

    }
}
