﻿using System;



namespace Robbiblubber.Util.Nuub
{
    /// <summary>Item base interface.</summary>
    public interface IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item ID.</summary>
        string ID { get; }


        /// <summary>Gets or sets the item name.</summary>
        string Name { get; set; }


        /// <summary>Gets or sets the item description.</summary>
        string Description { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Deletes the item.</summary>
        void Delete();

        
        /// <summary>Saves the item.</summary>
        void Save();


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        IItem CopyTo(IItem target);


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        IItem MoveTo(IItem target);
    }
}
