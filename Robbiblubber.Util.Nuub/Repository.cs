﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

using Robbiblubber.Util.Library;
using Robbiblubber.Data;
using System.Data.SQLite;

namespace Robbiblubber.Util.Nuub
{
    /// <summary>This class implements a Nuub repository.</summary>
    public sealed class Repository: IItem, IExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Connection.</summary>
        internal IDbConnection _Connection;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Repository(string file)
        {
            File = Path.GetFullPath(file);

            _Connection = new SQLiteConnection("Data Source=" + File + ";Version=3;");
            _Connection.Open();

            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads a repository.</summary>
        /// <param name="file">File name.</param>
        /// <returns>Repository.</returns>
        public static Repository Load(string file)
        {
            Repository rval = new Repository(file);

            DdpFile cfg = Ddp.Load(PathOp.UserSettingsFile);
            cfg.SetString("/settings/repository", file);
            cfg.Save();

            return rval;
        }


        /// <summary>Creates and loads a repository.</summary>
        /// <param name="file">File name.</param>
        /// <returns>Repsoitory.</returns>
        public static Repository Create(string file)
        {
            if(!Directory.Exists(Path.GetDirectoryName(file))) { Directory.CreateDirectory(Path.GetDirectoryName(file)); }

            if(System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
            }
            System.IO.File.Copy(PathOp.ApplicationDirectory + @"\repository.template", file);

            return Load(file);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the repository file name.</summary>
        public string File
        {
            get; private set;
        }


        /// <summary>Gets the solutions in this repository.</summary>
        public SolutionList Solutions
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes this instance.</summary>
        public void Refresh()
        {
            IDbCommand cmd = _Connection.CreateCommand("SELECT NAME, DESCRIPTION FROM REPOSITORY");
            IDataReader re = cmd.ExecuteReader();

            re.Read();
            Name = re.GetString(0);
            Description = re.GetString(1);

            Disposal.Recycle(re, cmd);

            Solutions = new SolutionList(this);
        }


        /// <summary>Adds a new solution to the repository.</summary>
        /// <returns>Solution.</returns>
        public Solution AddSolution()
        {
            return new Solution(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item ID.</summary>
        string IItem.ID
        {
            get { return File; }
        }


        /// <summary>Gets or sets the repository name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the repository description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Saves the repository details.</summary>
        public void Save()
        {
            IDbCommand cmd = _Connection.CreateCommand("UPDATE REPOSITORY SET NAME = :name, DESCRIPTION = :descr");
            cmd.AddParameter(":name", Name);
            cmd.AddParameter(":descr", Description);
            cmd.ExecuteNonQuery();
        }


        /// <summary>Deletes the item.</summary>
        void IItem.Delete()
        {
            throw new InvalidOperationException("Repository cannot be deleted.");
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        IItem IItem.CopyTo(IItem target)
        {
            return null;
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        IItem IItem.MoveTo(IItem target)
        {
            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an array of all packages in this repository.</summary>
        /// <returns>Packages array.</returns>
        internal Package[] _GetPackages()
        {
            List<Package> rval = new List<Package>();

            foreach(Solution i in Solutions)
            {
                rval.AddRange(i.Packages);
            }

            return rval.OrderBy(m => m.Name).ToArray();
        }


        /// <summary>Gets an object by its ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Returns the object with the given ID, returns NULL when no object with this ID has been found.</returns>
        internal object _ObjectByID(string id)
        {
            return _ObjectByID(this, id);
        }


        /// <summary>Searches the object hierarchy for an ID.</summary>
        /// <param name="obj">Root object.</param>
        /// <param name="id">ID.</param>
        /// <returns>Returns the object with the given ID, returns NULL when no object with this ID has been found.</returns>
        private object _ObjectByID(object obj, string id)
        {
            object rval = null;

            if(obj is Repository)
            {
                if(id == File) return this;

                foreach(Solution i in ((Repository) obj).Solutions)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }
            }
            else if(obj is Solution)
            {
                if(((Solution) obj).ID == id) { return obj; }

                foreach(Package i in ((Solution) obj).Packages)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }
            }
            else if(obj is Package)
            {
                if(((Package) obj).ID == id) { return obj; }

                foreach(Folder i in ((Package) obj).Folders)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }
            }
            else if(obj is Folder)
            {
                if(((Folder) obj).ID == id) { return obj; }

                foreach(Folder i in ((Folder) obj).Folders)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }

                foreach(CopyJob i in ((Folder) obj).Jobs)
                {
                    if(i.ID == id) return i;
                }
            }

            return null;
        }


        /// <summary>Gets a unique name.</summary>
        /// <param name="name">Base name.</param>
        /// <param name="table">Table name.</param>
        /// <returns>Unique name.</returns>
        internal string _GetUniqueName(string name, string table)
        {
            string rval = null;
            int n = 0;

            while(true)
            {
                rval = name;
                if(++n > 1) { rval += " (" + n.ToString() + ")"; }

                IDbCommand cmd = _Connection.CreateCommand("SELECT COUNT(*) FROM " + table + " WHERE NAME = :name");
                cmd.AddParameter(":name", rval);

                if(((long) cmd.ExecuteScalar()) == 0)
                {
                    Disposal.Recycle(cmd);
                    break;
                }
                Disposal.Dispose(cmd);
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExecutable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Builds this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Build(ExecutionMetadata meta)
        {
            foreach(Solution i in Solutions) { i.Build(meta); }
        }


        /// <summary>Deploys this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Deploy(ExecutionMetadata meta)
        {
            foreach(Solution i in Solutions) { i.Deploy(meta); }
        }
    }
}
